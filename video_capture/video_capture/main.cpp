#include "main.h"

using namespace std;
using namespace cv;

int main() {
	cap_flag = 0;

#if CAMERA_CAPTURE == 1
	cap.open(0);
#else
	cap.open(VIDEO_INPUT_DIR);
#endif

	cap.set(CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CAP_PROP_FRAME_HEIGHT, 480);

	cout << cap.get(CAP_PROP_FRAME_WIDTH) << " " << cap.get(CAP_PROP_FRAME_HEIGHT) << endl;
	cout << " s		-> stop/save" << endl;
	cout << " a		-> start" << endl;
	cout << " esc		-> quit" << endl;
	cout << " q		-> save settings" << endl;
	
	outputVideo.open(VIDEO_OUTPUT_DIR, CV_FOURCC('M', 'J', 'P', 'G'), 30, Size(640, 480));

	if (!cap.isOpened()) {
		cout << "Could not initialize camera capture...\n";
		return -1;
	}
	else
		cout << "video source opened \n";

	Mat ori;

	while (true) {
		cap.read(ori);

#if CAMERA_CAPTURE == 0
		if (ori.empty()) {
			cap.open(VIDEO_INPUT_DIR);
			cout << "video source opened \n";
			cap.read(ori);
		}
#endif

		imshow("ori", ori);
		if (cap_flag == 1)
			outputVideo << ori;			

		switch (waitKey(1)) {
		case 27:
			return 0;
			break;

		case 's':
			cout << "stop" << endl;
			cap_flag = 0;
			break;

		case 'a':
			cout << "start" << endl;
			cap_flag = 1;
			break;
		}
	}

	return 0;
}