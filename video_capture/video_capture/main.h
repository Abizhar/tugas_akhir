#pragma once

#ifndef MAIN_H_
#define MAIN_H_

#define CAMERA_CAPTURE 1

#define VIDEO_INPUT_DIR "v3_eded.avi"
#define VIDEO_OUTPUT_DIR "v3_ede.avi"

#include <iostream>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/tracking.hpp"

using namespace std;
using namespace cv;

VideoCapture cap;
VideoWriter outputVideo;

uint8_t cap_flag;
#endif
