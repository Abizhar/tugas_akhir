#ifndef MULTITHREADINGBASE_H_
#define MULTITHREADINGBASE_H_

#include <iostream>
#include <mutex>
#include <thread>

using namespace std;

class multiThreading_base {
   public:
    multiThreading_base();
    virtual ~multiThreading_base();

    int startThread();
    int waitThread();

    virtual void threadedFuntion();

    void basicLock();
    void basicUnlock();
    void quit();

    int running;

   private:
    thread th;
    mutex locker;

   protected:
};

#endif /* MULTITHREADINGBASE_H_ */
