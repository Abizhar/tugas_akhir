#ifndef CAM1_THREAD_H_
#define CAM1_THREAD_H_

#include "cam2_thread.h"
#include "compiler_setting.h"
#include "multiThreadingbase.h"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/videoio.hpp"
#include "periodic_call.h"

#include <ctype.h>
#include <iostream>

#define SPEED_MULTIPLIER 6

using namespace cv;
using namespace std;

typedef struct {
    double vx;
    double vy;
} vel;

typedef struct {
    double x;
    double y;
} pos;

class thread2 : public multiThreading_base {
   private:
    thread3 sub_thread;
    periodic_call cam1_timer;
    vel get_vel(vector<Point2f> *p0, vector<Point2f> *p1, vector<uchar> *status);
    double off_center_in, off_vertical_in;
    uint8_t line_detected_in;
    uint8_t line_status_in[REGION_SPLIT];

   public:
    uint8_t line_detected_out;
    uint8_t line_status_out[REGION_SPLIT];
    double off_center_out, off_vertical_out;
    vel velocity;
    double fps;
    void threadedFuntion();
};

#endif