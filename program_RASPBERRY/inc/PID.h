#pragma once
#include <vector>

using namespace std;

class mov_avg_PID {
   private:
    std::vector<double> arr;
    int sz = 0;
    int i  = 0;

   public:
    mov_avg_PID(int _sz);
    mov_avg_PID();
    double get_avg(double _in);
};

class PID {
   private:
    double error = 0, error_fil = 0, last_error = 0, last_input = 0;
    double upper_limit, lower_limit;
    double P, I, D, kp, ki, kd;
    double P_fil, I_fil, D_fil, in_fil;
    unsigned int P_fil_sz, I_fil_sz, D_fil_sz, in_fil_sz;
    mov_avg_PID P_mov_avg, I_mov_avg, D_mov_avg, in_mov_avg;

   public:
    double looptime = 1;
    double setPoint = 0, target = 0;

    bool dOnInput = false;
    bool no_limit = false;

    PID();
    PID(double _kp, double _ki, double _kd, double _sym_out_limit, double _looptime = 1);
    PID(vector<double> _kPID, double _sym_out_limit, double _looptime = 1);

    void set_limit(double _lower, double _upper);
    void set_limit(double _limit);
    void set_kPID(double _kp, double _ki, double _kd);
    void reset_Iterm();
    void remove_limit();
    void set_dOnInput();
    void set_dOnError();

    void set_P_fil_sz(unsigned int _sz);
    void set_I_fil_sz(unsigned int _sz);
    void set_D_fil_sz(unsigned int _sz);
    void set_in_fil_sz(unsigned int _sz);

    vector<double> get_PID();
    vector<double> get_PID_fil();
    double get_error();
    double get_error_fil();

    double compute(double _setPoint, double _input);
    double compute(double _input);
    ~PID();
};