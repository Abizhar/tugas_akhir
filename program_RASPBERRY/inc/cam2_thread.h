#ifndef CAM2_THREAD_H_
#define CAM2_THREAD_H_

#include "compiler_setting.h"
#include "multiThreadingbase.h"

#define CAMERA_CAPTURE 0
#define VIDEO_INPUT_DIR "v2_editted.avi"
#define DISPLAY_ROI_GRID 1
#define DISPLAY_REG_POINT 0
#define DISPLAY_EST_LINE 1
#define DISPLAY_CENTER_MARKER 0
#define DISPLAY_OFF_CENTER 0
#define DISPLAY_OFF_VERTICAL 0
#define THRESHOLD_WINDOW 0
#define DEBUG_COUT_PERIOD 30
#define LOG_ESTIMATED_LINE 1
#define LOG_FNAME "estimated_line.csv"
#define FILTER_SZ 5

#if defined(_WIN32)
#define SETTING_DIR "./../../settings/th_garis.ini"
#else
#define SETTING_DIR "./../../settings/th_garis.ini"
#endif

#include <iostream>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/tracking.hpp"

#include "Save_and_load_settings.h"
#include "log_file.h"
#include "movingAVG.h"
#include "periodic_call.h"

using namespace std;
using namespace cv;
using namespace bical;

typedef struct {
    double a;
    double b;
} line_des;

class thread3 : public multiThreading_base {
   private:
    VideoCapture cap;
    Mat garis, ori_hsv;

    Moments mments[REGION_SPLIT];
    Rect ROI_rect[REGION_SPLIT];
    Point centroids[REGION_SPLIT];
    Point tl[REGION_SPLIT];
    Point br[REGION_SPLIT];

    int frame_width, frame_height;

    uint8_t nan_flag;

    vector<int> th_garis;
    vector<Point> regression_point;
    line_des detected_line;
    line_des detected_line_filtered;
    line_des regression(vector<Point> _in);

    double off_center, off_vertical;

    mov_avg a, b;

    periodic_call timer1;

#if LOG_ESTIMATED_LINE == 1
    double log_time;
    log_file log_est_line;
#endif

   public:
    float fps;
    uint8_t line_detected;
    uint8_t line_status[REGION_SPLIT];
    Mat ori;
    double off_center_fil, off_vertical_fil;
    void threadedFuntion();
};
#endif