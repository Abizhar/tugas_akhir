#ifndef MAIN_H_
#define MAIN_H_
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "PID.h"
#include "Save_and_load_settings.h"
#include "cam1_thread.h"
#include "compiler_setting.h"
#include "log_file.h"
#include "periodic_call.h"
#include "stm32_comm.h"

#define POS_HOLD 0
#define SELF_ALIGN 1
#define CRUISE_FORWARD 2
#define CRUISE_BACKWARD 3

using namespace std;
using namespace bical;

thread1 STM32_comm_thread;
thread2 camera1_thread;
periodic_call timer1;
periodic_call PID_timer;

PID pitch;
PID roll_stab, roll_line;
PID yaw;

int16_t pitch_PID_sum = 0;
int16_t roll_PID_sum  = 0;
int16_t yaw_PID_sum   = 0;
int16_t ketinggian    = 0;

uint32_t test_var  = 0;
uint32_t loop_time = 0;

uint16_t RC_in_data[14];
uint16_t RC_out_data[14];
uint16_t ref_pitch, ref_roll, ref_yaw;
uint16_t main_state;
uint16_t end_of_line;

double cam1_fps;
double off_center, off_vertical;
double off_center_cm, off_vertical_cm;
double pitch_lim, roll_l_lim, roll_s_lim, yaw_lim;
double roll_s_filsz, pitch_filsz;
double cruise_fw_spd, cruise_bw_spd;
double off_center_th, off_vertical_th, end_of_line_max_th, end_of_line_min_th;
double divider;
double pos_hold_vx, pos_hold_vy;

vector<double> roll_stab_kPID, roll_line_kPID;
vector<double> pitch_kPID;
vector<double> yaw_kPID;
vector<double> settings_dat;
vector<string> settings_title;

vel velocity;
vel velocity_cm;
pos position;

void read_thread_data();
void write_thread_data();
void load_setting_file();
void open_log_file();
void save_log_file();

uint8_t line_detected = 0;
uint8_t line_status[REGION_SPLIT];
double roll_setPoint  = 0;
double pitch_setPoint = 0;

#if LOG_PID == 1
double log_time;
periodic_call log_timer;
vector<double> PID_roll_val, PID_pitch_val, PID_roll_fil_val, PID_pitch_fil_val;
log_file PID_log;
char file_name[50];
#endif

#endif