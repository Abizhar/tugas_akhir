#include "log_file.h"

log_file::log_file(char * _file_name) {
	file.open(_file_name, ios::out);
}
log_file::log_file(char * _file_name, vector<string> _title) {
	file.open(_file_name, ios::out);
	for (uint8_t i = 0; i < _title.size(); i++) {
		if (i == _title.size()-1)
			file << _title[i] << endl;
		else 
			file << _title[i] << ", ";
	}	 
}
log_file::log_file() {
}

int log_file::open_log(char * _file_name) {
	file.open(_file_name, ios::out);
	if (!file.is_open())
		return 1;
	else
		return 0;
}
int log_file::open_log(char * _file_name, vector<string> _title) {
	file.open(_file_name, ios::out);
	if (!file.is_open())
		return 0;

	for (uint8_t i = 0; i < _title.size(); i++) {
		if (i == _title.size()-1)
			file << _title[i] << endl;
		else
			file << _title[i] << ", ";
	}
	return 1;
}

int log_file::log(vector<double> _data) {
	for (uint8_t i = 0; i < _data.size(); i++) {
		if (i == _data.size()-1)
			file << _data[i] << endl;
		else
			file << _data[i] << ", ";
	}
	return 0;
}

int log_file::close_log() {
	file.close();
	return 0;
}


log_file::~log_file() {
	file.close();
}
