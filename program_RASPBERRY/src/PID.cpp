#include "PID.h"

mov_avg_PID::mov_avg_PID(int _sz) {
    sz  = _sz;
    arr = std::vector<double>(_sz);
}
mov_avg_PID::mov_avg_PID() {
    sz  = 1;
    arr = std::vector<double>(1);
}
double mov_avg_PID::get_avg(double _in) {
    arr[i] = _in;
    i++;

    if (i >= sz) {
        i = 0;
    }

    double sum = 0;

    for (int j = 0; j < sz; j++) {
        sum += arr[j];
    }

    return sum / sz;
}

PID::PID() {
    P      = 0;
    I      = 0;
    D      = 0;
    P_fil  = 0;
    I_fil  = 0;
    D_fil  = 0;
    in_fil = 0;

    P_fil_sz   = 1;
    P_mov_avg  = mov_avg_PID(P_fil_sz);
    I_fil_sz   = 1;
    I_mov_avg  = mov_avg_PID(I_fil_sz);
    D_fil_sz   = 1;
    D_mov_avg  = mov_avg_PID(D_fil_sz);
    in_fil_sz  = 1;
    in_mov_avg = mov_avg_PID(in_fil_sz);
}
PID::PID(double _kp, double _ki, double _kd, double _sym_out_limit, double _looptime) {
    error = 0;
    if (_sym_out_limit >= 0) {
        upper_limit = _sym_out_limit;
        lower_limit = -_sym_out_limit;
    } else
        no_limit = true;

    P      = 0;
    I      = 0;
    D      = 0;
    P_fil  = 0;
    I_fil  = 0;
    D_fil  = 0;
    in_fil = 0;
    kp     = _kp;
    ki     = _ki;
    kd     = _kd;

    this->looptime = _looptime;
    this->setPoint = 0, target = 0;
    this->dOnInput = false;
    P_fil_sz       = 1;
    P_mov_avg      = mov_avg_PID(P_fil_sz);
    I_fil_sz       = 1;
    I_mov_avg      = mov_avg_PID(I_fil_sz);
    D_fil_sz       = 1;
    D_mov_avg      = mov_avg_PID(D_fil_sz);
    in_fil_sz      = 1;
    in_mov_avg     = mov_avg_PID(in_fil_sz);
}
PID::PID(vector<double> _kPID, double _sym_out_limit, double _looptime) {
    error = 0;
    if (_sym_out_limit >= 0) {
        upper_limit = _sym_out_limit;
        lower_limit = -_sym_out_limit;
    } else
        no_limit = true;

    P      = 0;
    I      = 0;
    D      = 0;
    P_fil  = 0;
    I_fil  = 0;
    D_fil  = 0;
    in_fil = 0;

    kp = _kPID[0];
    ki = _kPID[1];
    kd = _kPID[2];

    this->looptime = _looptime;
    this->setPoint = 0, target = 0;
    this->dOnInput = false;

    P_fil_sz   = 1;
    P_mov_avg  = mov_avg_PID(P_fil_sz);
    I_fil_sz   = 1;
    I_mov_avg  = mov_avg_PID(I_fil_sz);
    D_fil_sz   = 1;
    D_mov_avg  = mov_avg_PID(D_fil_sz);
    in_fil_sz  = 1;
    in_mov_avg = mov_avg_PID(in_fil_sz);
}

double PID::compute(double _setPoint, double _input) {
    this->setPoint = _setPoint;
    error          = this->setPoint - _input;
    in_fil         = in_mov_avg.get_avg(_input);
    error_fil      = this->setPoint - in_fil;

    P     = kp * error_fil;
    P_fil = P_mov_avg.get_avg(P);
    if (dOnInput) {
        //D = -kd* (_input - last_input) / looptime;
        D     = kd * (last_input - in_fil) / looptime;
        D_fil = D_mov_avg.get_avg(D);
    } else {
        D     = kd * (error_fil - last_error) / looptime;
        D_fil = D_mov_avg.get_avg(D);
    }

    last_error = error_fil;
    last_input = in_fil;

    double PID = P_fil + D_fil;

    if (no_limit) {  //if output is not capped
        I += ki * error_fil * looptime;
        I_fil = I_mov_avg.get_avg(I);
        return PID + I_fil;
    } else {  //if output is capped at a value
        if (PID >= upper_limit)
            return upper_limit;
        else if (PID <= lower_limit)
            return lower_limit;

        I += ki * error_fil * looptime;

        if (I >= upper_limit)
            I = upper_limit;
        else if (I <= lower_limit)
            I = lower_limit;

        I_fil = I_mov_avg.get_avg(I);
        PID += I_fil;
        if (PID >= upper_limit)
            return upper_limit;
        else if (PID <= lower_limit)
            return lower_limit;

        return PID;
    }
}
double PID::compute(double _input) {
    error     = this->setPoint - _input;
    in_fil    = in_mov_avg.get_avg(_input);
    error_fil = this->setPoint - in_fil;

    P     = kp * error_fil;
    P_fil = P_mov_avg.get_avg(P);
    if (dOnInput) {
        //D = -kd* (_input - last_input) / looptime;
        D     = kd * (last_input - in_fil) / looptime;
        D_fil = D_mov_avg.get_avg(D);
    } else {
        D     = kd * (error_fil - last_error) / looptime;
        D_fil = D_mov_avg.get_avg(D);
    }

    last_error = error_fil;
    last_input = in_fil;

    double PID = P_fil + D_fil;

    if (no_limit) {  //if output is not capped
        I += ki * error_fil * looptime;
        I_fil = I_mov_avg.get_avg(I);
        return PID + I_fil;
    } else {  //if output is capped at a value
        if (PID >= upper_limit)
            return upper_limit;
        else if (PID <= lower_limit)
            return lower_limit;

        I += ki * error_fil * looptime;
        I_fil = I_mov_avg.get_avg(I);

        if (I >= upper_limit) {
            I     = upper_limit;
            I_fil = upper_limit;
        } else if (I <= lower_limit) {
            I     = lower_limit;
            I_fil = lower_limit;
        }

        PID += I_fil;
        if (PID >= upper_limit)
            return upper_limit;
        else if (PID <= lower_limit)
            return lower_limit;

        return PID;
    }
}

void PID::set_limit(double _lower, double _upper) {  //set unsymetrical limit
    upper_limit = _upper;
    lower_limit = _lower;
    no_limit    = true;
}
void PID::set_limit(double _limit) {  //set symetrical limit
    upper_limit = _limit;
    lower_limit = -_limit;
    no_limit    = true;
}
void PID::set_kPID(double _kp, double _ki, double _kd) {
    kp = _kp;
    ki = _ki;
    kd = _kd;
}
vector<double> PID::get_PID() {
    return vector<double>(3) = {P, I, D};
}
vector<double> PID::get_PID_fil() {
    return vector<double>(3) = {P_fil, I_fil, D_fil};
}
double PID::get_error() {
    return error;
}
double PID::get_error_fil() {
    return error_fil;
}

void PID::reset_Iterm() {
    I = 0;
}
void PID::remove_limit() {
    no_limit = true;
}
void PID::set_dOnInput() {
    dOnInput = true;
}
void PID::set_dOnError() {
    dOnInput = false;
}
void PID::set_P_fil_sz(unsigned int _sz) {
    P_fil_sz  = _sz;
    P_mov_avg = mov_avg_PID(P_fil_sz);
}
void PID::set_I_fil_sz(unsigned int _sz) {
    I_fil_sz  = _sz;
    I_mov_avg = mov_avg_PID(I_fil_sz);
}
void PID::set_D_fil_sz(unsigned int _sz) {
    D_fil_sz  = _sz;
    D_mov_avg = mov_avg_PID(D_fil_sz);
}
void PID::set_in_fil_sz(unsigned int _sz) {
    in_fil_sz  = _sz;
    in_mov_avg = mov_avg_PID(in_fil_sz);
}

PID::~PID() {
}
