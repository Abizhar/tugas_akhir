#include "cam2_thread.h"

using namespace std;
using namespace bical;

void thread3::threadedFuntion() {
    th_garis = vector<int>(6);
    if (!load_settings(SETTING_DIR, &th_garis)) {
        cout << "file not found, creating new file \n";
        save_settings(SETTING_DIR, th_garis);
    }

    a = mov_avg(FILTER_SZ);
    b = mov_avg(FILTER_SZ);

#if LOG_ESTIMATED_LINE == 1
    log_time = 0;
    vector<string> title;
    title.push_back(string("time"));
    title.push_back(string("a_raw"));
    title.push_back(string("a_fil"));
    title.push_back(string("b_raw"));
    title.push_back(string("b_fil"));
    title.push_back(string("off_vertical_raw"));
    title.push_back(string("off_vertical_fil"));
    title.push_back(string("off_center_raw"));
    title.push_back(string("off_center_fil"));
    log_est_line.open_log(LOG_FNAME, title);
#endif  // LOG_ESTIMATED_LINE == 1

    //block jika ori masih kosong//
    bool frame_status = 1;

    while (frame_status) {
        this->basicLock();
        frame_status = this->ori.empty();
        this->basicUnlock();
    }

    //block jika ori masih kosong//
    this->basicLock();
    frame_width  = this->ori.size().width;
    frame_height = this->ori.size().height;
    this->basicUnlock();

    cout << th_garis[0] << " " << th_garis[1] << " " << th_garis[2]
         << " " << th_garis[3] << " " << th_garis[4] << " " << th_garis[5]
         << " adwdaw" << endl;

    nan_flag = 1;
    while (this->running == 1) {
        //sinkronasi thread////////////////////////////
        //fps calculation
        uint32_t time = timer1.elapsed();
        if (time > 0)
            fps = 1000 / time;
        //fps calculation

        this->basicLock();
        cvtColor(this->ori, ori_hsv, CV_BGR2HSV);
        this->basicUnlock();

        inRange(ori_hsv, Scalar(th_garis[0], th_garis[1], th_garis[2]), Scalar(th_garis[3], th_garis[4], th_garis[5]), garis);

        regression_point = vector<Point>(0);

        //gathering regression point
        for (uint8_t i = 0; i < REGION_SPLIT; i++) {
            if (i == 0) {  //jika region pertama
                tl[i]       = Point(0, frame_height * i / REGION_SPLIT);
                br[i]       = Point(frame_width, frame_height * (i + 1) / REGION_SPLIT);
                ROI_rect[i] = Rect(tl[i], br[i]);
                mments[i]   = moments(garis(ROI_rect[i]), true);
            } else {
                if (mments[i - 1].m00 > MIN_PIXEL_COUNT) {  //jika sebelumnya ditemukan garis
                    int left  = 0;
                    int right = 0;

                    left  = centroids[i - 1].x - (frame_width * REGION_WIDTH_RATIO * 0.5);
                    right = centroids[i - 1].x + (frame_width * REGION_WIDTH_RATIO * 0.5);
                    if (left < 0)
                        left = 0;
                    if (right > frame_width)
                        right = frame_width;

                    tl[i]       = Point(left, frame_height * i / REGION_SPLIT);
                    br[i]       = Point(right, frame_height * (i + 1) / REGION_SPLIT);
                    ROI_rect[i] = Rect(tl[i], br[i]);

                    mments[i] = moments(garis(ROI_rect[i]), true);
                    if (mments[i].m00 <= MIN_PIXEL_COUNT) {
                        tl[i]       = Point(0, frame_height * i / REGION_SPLIT);
                        br[i]       = Point(frame_width, frame_height * (i + 1) / REGION_SPLIT);
                        ROI_rect[i] = Rect(tl[i], br[i]);
                        mments[i]   = moments(garis(ROI_rect[i]), true);
                    }
                } else {  //jika tidak, cari full region
                    tl[i]       = Point(0, frame_height * i / REGION_SPLIT);
                    br[i]       = Point(frame_width, frame_height * (i + 1) / REGION_SPLIT);
                    ROI_rect[i] = Rect(tl[i], br[i]);
                    mments[i]   = moments(garis(ROI_rect[i]), true);
                }
            }

            if (mments[i].m00 > MIN_PIXEL_COUNT) {
                centroids[i] = Point((mments[i].m10 / mments[i].m00) + tl[i].x, (mments[i].m01 / mments[i].m00) + tl[i].y);
                regression_point.push_back(centroids[i]);
            }
        }

        this->basicLock();
        for (uint8_t i = 0; i < REGION_SPLIT; i++) {
            if (mments[i].m00 > MIN_PIXEL_COUNT) {
                this->line_status[i] = 255;
            } else {
                this->line_status[i] = 0;
            }
        }
        this->basicUnlock();
        //gathering regression point

        //line estimation
        if (regression_point.size() >= LINE_FOUND_THRESH) {
            detected_line = regression(regression_point);
            if (isnan(detected_line.a)) {
                if (!nan_flag) {
                    for (int i = 0; i < FILTER_SZ; i++) {
                        detected_line_filtered.a = a.get_avg(detected_line.a);
                        detected_line_filtered.b = b.get_avg(detected_line.b);
                    }
                } else {
                    detected_line_filtered.a = a.get_avg(detected_line.a);
                    detected_line_filtered.b = b.get_avg(detected_line.b);
                }
                nan_flag = 1;
            } else {
                if (nan_flag) {
                    for (int i = 0; i < FILTER_SZ; i++) {
                        detected_line_filtered.a = a.get_avg(detected_line.a);
                        detected_line_filtered.b = b.get_avg(detected_line.b);
                    }
                } else {
                    detected_line_filtered.a = a.get_avg(detected_line.a);
                    detected_line_filtered.b = b.get_avg(detected_line.b);
                }
                nan_flag = 0;
            }
            this->basicLock();
            this->line_detected = 1;
            this->basicUnlock();
        } else {
            this->basicLock();
            this->line_detected = 0;
            this->basicUnlock();
        }
        //line estimation

        //calculate off_center & off_vertical
        off_center   = (frame_width / 2) - (((frame_height / 2) - detected_line.b) / detected_line.a);
        off_vertical = (((frame_height / 2) - detected_line.b) / detected_line.a) - ((0 - detected_line.b) / detected_line.a);

        if (regression_point.size() >= LINE_FOUND_THRESH) {
            this->basicLock();
            if (isnan(detected_line_filtered.a)) {
                off_vertical_fil = 0;
                off_center_fil   = (frame_width / 2) - detected_line_filtered.b;
            } else {
                off_center_fil   = (frame_width / 2) - (((frame_height / 2) - detected_line_filtered.b) / detected_line_filtered.a);
                off_vertical_fil = (((frame_height / 2) - detected_line_filtered.b) / detected_line_filtered.a) - ((0 - detected_line_filtered.b) / detected_line_filtered.a);
            }
            this->basicUnlock();
        }
        //calculate off_center & off_vertical
        //cout << off_vertical_fil << " " << detected_line_filtered.b << " " << detected_line_filtered.a << " << " << off_center_fil << endl;
#if LOG_ESTIMATED_LINE == 1
        vector<double> line_data;
        line_data.push_back(log_time);
        line_data.push_back(detected_line.a);
        line_data.push_back(detected_line_filtered.a);
        line_data.push_back(detected_line.b);
        line_data.push_back(detected_line_filtered.b);
        this->basicLock();
        line_data.push_back(off_vertical);
        line_data.push_back(off_vertical_fil);
        line_data.push_back(off_center);
        line_data.push_back(off_center_fil);
        this->basicUnlock();
        log_est_line.log(line_data);
        log_time += DEBUG_COUT_PERIOD;
#endif
    }
    waitKey(1);
}

line_des thread3::regression(vector<Point> _in) {
    double sigXi   = 0;
    double sigYi   = 0;
    double sigXisq = 0;
    double sigXiYi = 0;
    double Xbar    = 0;
    double Ybar    = 0;

    for (uint8_t i = 0; i < _in.size(); i++) {
        sigXi += _in[i].x;
        sigYi += _in[i].y;
        sigXisq += (_in[i].x * _in[i].x);
        sigXiYi += (_in[i].x * _in[i].y);
    }

    Xbar = sigXi / _in.size();
    Ybar = sigYi / _in.size();

    line_des ret_val;
    ret_val.a = ((_in.size() * sigXiYi) - (sigXi * sigYi)) / ((_in.size() * sigXisq) - (sigXi * sigXi));

    if (isnan(ret_val.a)) {
        ret_val.b = Xbar;
    } else {
        ret_val.b = Ybar - (ret_val.a * Xbar);
    }
    return ret_val;
}