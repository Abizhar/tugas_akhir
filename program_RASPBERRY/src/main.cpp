#include "main.h"

using namespace std;

int main() {
    STM32_comm_thread.startThread();
    camera1_thread.startThread();

    load_setting_file();

    roll_stab = PID(roll_stab_kPID, roll_s_lim, PID_PERIODE);
    roll_line = PID(roll_line_kPID, roll_l_lim, PID_PERIODE);
    pitch     = PID(pitch_kPID, pitch_lim, PID_PERIODE);
    yaw       = PID(yaw_kPID, yaw_lim, PID_PERIODE);

    roll_stab.set_in_fil_sz(roll_s_filsz);
    pitch.set_in_fil_sz(pitch_filsz);

    cout << showpos << fixed;
    main_state = 0;

#if LOG_PID == 1
    open_log_file();
#endif

    while (true) {
        read_thread_data();
        loop_time = timer1.elapsed();
        position.x += (velocity.vx) * 10;
        position.y += (velocity.vy) * 10;

        if (RC_in_data[8] == 1200) {
            roll_stab.reset_Iterm();
            roll_line.reset_Iterm();
            pitch.reset_Iterm();
            yaw.reset_Iterm();

            ref_roll  = RC_in_data[0];
            ref_pitch = RC_in_data[1];
            ref_yaw   = RC_in_data[3];
        }

        end_of_line = 0;
        for (uint8_t i = REGION_SPLIT; i > 0; i--) {
            if (line_status[i - 1] == 255)
                break;
            end_of_line++;
        }

        ///////////////////////////////////main_program///////////////////////////////////
        if (PID_timer.call(PID_PERIODE)) {
            if (!line_detected)
                //jika tidak mendeteksi garis maka pos_hold
                main_state = POS_HOLD;
            else {
                if (abs(off_center) > off_center_th || abs(off_vertical) > off_vertical_th)
                    // jika belum sejajar dg garis maka self_align
                    main_state = SELF_ALIGN;
                else if (end_of_line < end_of_line_min_th)
                    // jika sejajar maka forward_cruise
                    main_state = CRUISE_FORWARD;
                else if (end_of_line >= end_of_line_min_th && end_of_line <= end_of_line_max_th)
                    // jika diujung garis maka self_align
                    main_state = SELF_ALIGN;
                else if (end_of_line > end_of_line_max_th)
                    // jika mendahului garis maka reverse_cruise
                    main_state = CRUISE_BACKWARD;
                else
                    //--//
                    main_state = POS_HOLD;
            }
            //overwrite main state here
            //main_state = POS_HOLD;
            //overwrite main state here

            yaw_PID_sum = (int)yaw.compute(0, off_vertical);
            switch (main_state) {
                case POS_HOLD:
                    roll_setPoint  = pos_hold_vx;
                    pitch_setPoint = pos_hold_vy;
                    yaw_PID_sum    = 0;
                    break;
                case SELF_ALIGN:
                    roll_setPoint  = -(int)roll_line.compute(0, off_center);
                    pitch_setPoint = 0;
                    break;
                case CRUISE_FORWARD:
                    roll_setPoint  = -(int)roll_line.compute(0, off_center);
                    pitch_setPoint = cruise_fw_spd;
                    break;
                case CRUISE_BACKWARD:
                    roll_setPoint  = -(int)roll_line.compute(0, off_center);
                    pitch_setPoint = cruise_bw_spd;
                    break;
            }
            pitch_PID_sum = (int)pitch.compute(pitch_setPoint, velocity.vy);
            roll_PID_sum  = (int)roll_stab.compute(roll_setPoint, velocity.vx);
        }
        ///////////////////////////////////main_program///////////////////////////////////

        RC_out_data[0] = ref_roll + roll_PID_sum;
        RC_out_data[1] = ref_pitch - pitch_PID_sum;
        RC_out_data[2] = RC_in_data[2];
        RC_out_data[3] = ref_yaw + yaw_PID_sum;
        RC_out_data[4] = RC_in_data[4];
        RC_out_data[5] = RC_in_data[5];
        RC_out_data[6] = RC_in_data[6];
        RC_out_data[7] = RC_in_data[7];
        RC_out_data[8] = RC_in_data[8];
        RC_out_data[9] = RC_in_data[9];

        if (timer1.call(20) == 1) {
            //cout << ketinggian << " " << off_center << " " << off_vertical << " " << velocity.vx << " " << velocity.vy << " " << roll_setPoint << endl;
            cout << ketinggian << " " << velocity.vx << " " << velocity.vy << endl;
        }

#if LOG_PID == 1
        if (log_timer.call(PID_PERIODE / LOG_RATIO)) {
            save_log_file();
        }
#endif

        write_thread_data();
    }
#if LOG_PID == 1
    PID_log.close_log();
#endif
    return 0;
}

void read_thread_data() {
    STM32_comm_thread.basicLock();
    for (int i = 0; i < 14; i++) {
        RC_in_data[i] = STM32_comm_thread.RC_in_data[i];
        ketinggian    = STM32_comm_thread.RC_in_data[10];
        if (ketinggian < 30) ketinggian = 30;
        //if (ketinggian > 300) ketinggian = 300;
    }
    divider = 278.5246148 * exp(-0.01911796479 * ketinggian);
    divider += 30;
    divider = 28.8 / divider;
    STM32_comm_thread.basicUnlock();

    camera1_thread.basicLock();
    cam1_fps       = camera1_thread.fps;
    velocity       = camera1_thread.velocity;
    velocity_cm.vx = velocity.vx * divider;
    velocity_cm.vy = velocity.vy * divider;
    velocity_cm.vx *= cam1_fps;
    velocity_cm.vy *= cam1_fps;
    off_center    = camera1_thread.off_center_out;
    off_center_cm = 0.5 * divider * off_center;
    off_vertical  = camera1_thread.off_vertical_out;
    line_detected = camera1_thread.line_detected_out;
    for (uint8_t i = 0; i < REGION_SPLIT; i++) {
        line_status[i] = camera1_thread.line_status_out[i];
    }
    camera1_thread.basicUnlock();
}
void write_thread_data() {
    STM32_comm_thread.basicLock();
    for (int i = 0; i < 14; i++) {
        STM32_comm_thread.RC_out_data[i] = RC_out_data[i];
    }
    STM32_comm_thread.basicUnlock();

    camera1_thread.basicLock();
    camera1_thread.basicUnlock();
}
void load_setting_file() {
    settings_dat   = vector<double>(26);
    settings_title = vector<string>(26);
    load_settings("settings.ini", &settings_title, &settings_dat);

    roll_stab_kPID = vector<double>(3);
    roll_line_kPID = vector<double>(3);
    pitch_kPID     = vector<double>(3);
    yaw_kPID       = vector<double>(3);

    //assign setting val
    cout << "__________________________________SETTINGS.INI__________________________________" << endl
         << endl
         << endl;

    roll_stab_kPID[0] = settings_dat[5];
    roll_stab_kPID[1] = settings_dat[6];
    roll_stab_kPID[2] = settings_dat[7];
    cout << "roll_stab_kPID " << roll_stab_kPID[0] << " " << roll_stab_kPID[1] << " " << roll_stab_kPID[2] << endl;

    roll_line_kPID[0] = settings_dat[10];
    roll_line_kPID[1] = settings_dat[11];
    roll_line_kPID[2] = settings_dat[12];
    cout << "roll_line_kPID " << roll_line_kPID[0] << " " << roll_line_kPID[1] << " " << roll_line_kPID[2] << endl;

    pitch_kPID[0] = settings_dat[0];
    pitch_kPID[1] = settings_dat[1];
    pitch_kPID[2] = settings_dat[2];
    cout << "pitch_kPID " << pitch_kPID[0] << " " << pitch_kPID[1] << " " << pitch_kPID[2] << endl;

    yaw_kPID[0] = settings_dat[14];
    yaw_kPID[1] = settings_dat[15];
    yaw_kPID[2] = settings_dat[16];
    cout << "yaw_kPID " << yaw_kPID[0] << " " << yaw_kPID[1] << " " << yaw_kPID[2] << endl;

    roll_l_lim = settings_dat[13];
    roll_s_lim = settings_dat[8];
    pitch_lim  = settings_dat[3];
    yaw_lim    = settings_dat[17];
    cout << "roll_l_lim " << roll_l_lim << endl;
    cout << "roll_s_lim " << roll_s_lim << endl;
    cout << "pitch_lim " << pitch_lim << endl;
    cout << "yaw_lim " << yaw_lim << endl;

    pitch_filsz  = settings_dat[4];
    roll_s_filsz = settings_dat[9];
    cout << "pitch_filsz " << pitch_filsz << endl;
    cout << "roll_s_filsz " << roll_s_filsz << endl;

    cruise_fw_spd = settings_dat[18];
    cruise_bw_spd = settings_dat[19];
    cout << "cruise_fw_spd " << cruise_fw_spd << endl;
    cout << "cruise_fw_spd " << cruise_fw_spd << endl;

    off_center_th   = settings_dat[20];
    off_vertical_th = settings_dat[21];
    cout << "off_center_th " << off_center_th << endl;
    cout << "off_vertical_th " << off_vertical_th << endl;

    end_of_line_max_th = settings_dat[22];
    end_of_line_min_th = settings_dat[23];
    cout << "end_of_line_max_th " << end_of_line_max_th << endl;
    cout << "end_of_line_min_th " << end_of_line_min_th << endl;

    pos_hold_vx = settings_dat[24];
    pos_hold_vy = settings_dat[25];
    cout << "pos_hold_vx " << pos_hold_vx << endl;
    cout << "pos_hold_vy " << pos_hold_vy << endl;
    cout
        << "__________________________________SETTINGS.INI__________________________________" << endl
        << endl
        << endl;
    //assign setting val
}
void open_log_file() {
    vector<string> log_title;
    //log_title.push_back(string("time"));
    // log_title.push_back(string("P_pitch"));
    // log_title.push_back(string("I_pitch"));
    // log_title.push_back(string("D_pitch"));
    // log_title.push_back(string("error_pitch"));
    // log_title.push_back(string("error_fil_pitch"));
    // log_title.push_back(string("PID_sum_pitch"));
    // log_title.push_back(string("input_pitch"));

    // log_title.push_back(string("P_roll"));
    // log_title.push_back(string("I_roll"));
    // log_title.push_back(string("D_roll"));
    // log_title.push_back(string("error_roll"));
    // log_title.push_back(string("error_fil_roll"));
    // log_title.push_back(string("PID_sum_roll"));
    // log_title.push_back(string("input_roll"));

    // log_title.push_back(string("P_yaw"));
    // log_title.push_back(string("I_yaw"));
    // log_title.push_back(string("D_yaw"));
    // log_title.push_back(string("error_yaw"));
    // log_title.push_back(string("error_fil_yaw"));
    // log_title.push_back(string("PID_sum_yaw"));
    // log_title.push_back(string("input_yaw"));

    // log_title.push_back(string("off_center"));
    // log_title.push_back(string("off_vertical"));

    ///////////////////////////////////////////////////
    // log_title.push_back(string("time"));
    // log_title.push_back(string("vx"));
    // log_title.push_back(string("vy"));
    // log_title.push_back(string("ketinggian"));
    // log_title.push_back(string("control"));
    // log_title.push_back(string("P_pitch"));
    // log_title.push_back(string("I_pitch"));
    // log_title.push_back(string("D_pitch"));
    // log_title.push_back(string("error_pitch"));
    // log_title.push_back(string("error_fil_pitch"));
    // log_title.push_back(string("PID_sum_pitch"));
    // log_title.push_back(string("input_pitch"));
    ///////////////////////////////////////////////////
    log_title.push_back(string("time"));
    log_title.push_back(string("ketinggian"));
    log_title.push_back(string("vx"));
    log_title.push_back(string("vy"));
    log_title.push_back(string("off_center"));
    log_title.push_back(string("off_vertical"));
    log_title.push_back(string("roll_setpoint"));
    log_title.push_back(string("pitch_setpoint"));
    ///////////////////////////////////////////////////
    // snprintf(file_name, 50, "%.2f_%.2f_%.2f_center_vertical.csv", pitch_kPID[0], pitch_kPID[1], pitch_kPID[2]);
    snprintf(file_name, 50, "PID sudut_%.2f.csv", cruise_bw_spd);
    PID_log.open_log(file_name, log_title);
}
void save_log_file() {
    vector<double> data;
    // data.push_back(log_time);

    // data.push_back(pitch.get_PID()[0]);
    // data.push_back(pitch.get_PID()[1]);
    // data.push_back(pitch.get_PID()[2]);
    // data.push_back(pitch.get_error());
    // data.push_back(pitch.get_error_fil());
    // data.push_back(pitch_PID_sum);
    // data.push_back(velocity.vy);

    // data.push_back(roll_stab.get_PID()[0]);
    // data.push_back(roll_stab.get_PID()[1]);
    // data.push_back(roll_stab.get_PID()[2]);
    // data.push_back(roll_stab.get_error());
    // data.push_back(roll_stab.get_error_fil());
    // data.push_back(roll_PID_sum);
    // data.push_back(velocity.vx);

    // data.push_back(yaw.get_PID()[0]);
    // data.push_back(yaw.get_PID()[1]);
    // data.push_back(yaw.get_PID()[2]);
    // data.push_back(yaw.get_error());
    // data.push_back(yaw.get_error_fil());
    // data.push_back(yaw_PID_sum);
    // data.push_back(off_center);

    // data.push_back(off_center);
    // data.push_back(off_vertical);
    ///////////////////////////////////////////////////
    // data.push_back(log_time);
    // data.push_back(velocity.vx);
    // data.push_back(velocity.vy);
    // data.push_back(ketinggian);
    // data.push_back(RC_in_data[8]);
    // data.push_back(pitch.get_PID()[0]);
    // data.push_back(pitch.get_PID()[1]);
    // data.push_back(pitch.get_PID()[2]);
    // data.push_back(pitch.get_error());
    // data.push_back(pitch.get_error_fil());
    // data.push_back(pitch_PID_sum);
    // data.push_back(velocity.vy);
    ///////////////////////////////////////////////////
    data.push_back(log_time);
    data.push_back(ketinggian);
    data.push_back(velocity_cm.vx);
    data.push_back(velocity_cm.vy);
    data.push_back(off_center_cm);
    data.push_back(off_vertical);
    data.push_back(roll_setPoint);
    data.push_back(pitch_setPoint);
    ///////////////////////////////////////////////////
    PID_log.log(data);
    log_time += PID_PERIODE / LOG_RATIO;
}