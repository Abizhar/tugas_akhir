#include "cam1_thread.h"

void thread2::threadedFuntion() {
    sub_thread.startThread();

    cam1_timer = periodic_call();
    VideoCapture cap;
    TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
    Size subPixWinSize(10, 10), winSize(31, 31);

    const int MAX_COUNT = 10;
    bool nightMode      = false;

    cap.open(0);
    cap.set(CAP_PROP_FRAME_WIDTH, 640);
    cap.set(CAP_PROP_FRAME_HEIGHT, 480);
    cout << cap.get(CAP_PROP_FRAME_WIDTH) << " " << cap.get(CAP_PROP_FRAME_HEIGHT) << endl;

    if (!cap.isOpened()) {
        cout << "Could not initialize capturing...\n";
        this->quit();
    }

    //namedWindow("LK Demo", 1);

    Mat gray, prevGray, image, frame;
    vector<Point2f> points[2];

    while (this->running == 1) {
        sub_thread.basicLock();
        off_center_in    = sub_thread.off_center_fil;
        off_vertical_in  = sub_thread.off_vertical_fil;
        line_detected_in = sub_thread.line_detected;
        for (uint8_t i = 0; i < REGION_SPLIT; i++) {
            line_status_in[i] = sub_thread.line_status[i];
        }
        sub_thread.basicUnlock();

        this->basicLock();
        this->off_center_out    = off_center_in;
        this->off_vertical_out  = off_vertical_in;
        this->line_detected_out = line_detected_in;
        for (uint8_t i = 0; i < REGION_SPLIT; i++) {
            this->line_status_out[i] = line_status_in[i];
        }
        this->basicUnlock();

        cap >> image;
        resize(image, image, Size(320, 240));

        sub_thread.basicLock();
        resize(image, sub_thread.ori, Size(160, 120));
        //image.copyTo(sub_thread.ori);
        sub_thread.basicUnlock();

        cvtColor(image, gray, COLOR_BGR2GRAY);

        if (!points[0].empty()) {  // jika point[0] tidak kosong
            vector<uchar> status;
            vector<float> err;

            if (prevGray.empty())
                gray.copyTo(prevGray);

            calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);

            this->basicLock();
            this->fps      = 1000 / cam1_timer.elapsed();
            this->velocity = get_vel(&points[0], &points[1], &status);
            this->basicUnlock();

            size_t i, k;
            for (i = k = 0; i < points[1].size(); i++) {
                if (!status[i])
                    continue;

                //arrowedLine(image, points[0][i], points[1][i], Scalar(255, 0, 255), 8, 0, 0.4, 0.4);
                points[1][k++] = points[1][i];
            }
            points[1].resize(k);
        }

        goodFeaturesToTrack(gray, points[1], MAX_COUNT, 0.01, 10);
        cornerSubPix(gray, points[1], subPixWinSize, Size(-1, -1), termcrit);

        //imshow("LK Demo", image);

        char c = (char)waitKey(10);
        if (c == 27)
            break;
        switch (c) {
            case 'n':
                nightMode = !nightMode;
                break;
        }

        std::swap(points[1], points[0]);
        cv::swap(prevGray, gray);
    }

    this->quit();
}

vel thread2::get_vel(vector<Point2f> *p0, vector<Point2f> *p1, vector<uchar> *status) {
    vel velocity;
    velocity.vx = 0;
    velocity.vy = 0;
    int k       = 0;
    for (uint16_t i = 0; i < p1->size(); i++) {
        if (!status->at(i))
            continue;

        velocity.vx += (p1->at(i).x - p0->at(i).x);
        velocity.vy += (p1->at(i).y - p0->at(i).y);
        k++;
    }

    velocity.vx *= SPEED_MULTIPLIER;
    velocity.vy *= SPEED_MULTIPLIER;

    if (k == 0) {
        velocity.vx = 0;
        velocity.vy = 0;
        return velocity;
    }

    velocity.vx /= k;
    velocity.vy /= k;

    return velocity;
}