# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/PID.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/program_RASPI.dir/src/PID.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/Save_and_load_settings.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/program_RASPI.dir/src/Save_and_load_settings.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/cam1_thread.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/program_RASPI.dir/src/cam1_thread.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/main.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/program_RASPI.dir/src/main.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/multiThreadingbase.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/program_RASPI.dir/src/multiThreadingbase.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/priodic_call.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/program_RASPI.dir/src/priodic_call.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/stm32_comm.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/program_RASPI.dir/src/stm32_comm.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "/usr/local/include/opencv"
  ".././inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
