# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/main.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/test_raspicam.dir/src/main.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/multiThreadingbase.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/test_raspicam.dir/src/multiThreadingbase.cpp.o"
  "/home/pi/GIT/tugas_akhir/program_RASPBERRY/src/stm32_comm.cpp" "/home/pi/GIT/tugas_akhir/program_RASPBERRY/build/CMakeFiles/test_raspicam.dir/src/stm32_comm.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "/usr/local/include/opencv"
  ".././inc"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
