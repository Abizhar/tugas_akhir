#include <vector>

class mov_avg{
  private:
  std::vector<double> arr;
  int sz = 0;
  int i = 0;
  
  public:
  mov_avg(int _sz);
  mov_avg();
  double get_avg(double _in);
};

