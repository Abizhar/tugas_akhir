#ifndef MULTITHREADINGBASE_H_
#define MULTITHREADINGBASE_H_

#include <thread>
#include <mutex>
#include <iostream>

using namespace std;

class multiThreading_base {
public:
	multiThreading_base();
	virtual ~multiThreading_base();

	int startThread();
	int waitThread();

	virtual void threadedFuntion();

	void basicLock();
	void basicUnlock();
	void quit();
	void synch_thread();
	void synch_start();
	void synch_stop();

	int running = 0;
	bool busy = 0;
	bool run = 1;
private:
	thread th;
	mutex locker;
protected:
	
};

#endif /* MULTITHREADINGBASE_H_ */
