#include "multiThreadingbase.h"

multiThreading_base::multiThreading_base() {
	this->running = 0;
}

multiThreading_base::~multiThreading_base() {
	this->running = 0;

	if(th.joinable()){
		th.join();
	}
}

int multiThreading_base::startThread(){ // ret 1 if successuflly start a new thread, 0 if thread already running
	if(!running){
		this->running = 1;
		th = thread(&multiThreading_base::threadedFuntion, this);
		return 1;
	}
	else
		return 0;
}

int multiThreading_base::waitThread(){
	this->running = 0;
		if(th.joinable()){
			th.join();
			return 1;
		}
		else
			return 0;
}

void multiThreading_base::threadedFuntion(){

}

void multiThreading_base::basicLock(){
	locker.lock();
}
void multiThreading_base::basicUnlock(){
	locker.unlock();
}
void multiThreading_base::quit() {
	this->running = 0;
}
void multiThreading_base::synch_thread() {
	bool _busy = 1;

	while (_busy) {
		this->basicLock();
		_busy = this->busy;
		this->basicUnlock();
	}
	this->basicLock();
	this->run = 1;
	this->basicUnlock();
}
void multiThreading_base::synch_start() {
	bool _run = 0;
	
	while (!_run) {
		this->basicLock();
		_run = this->run;
		
		if (!this->running)
			this->quit();
		this->basicUnlock();
	}

	this->basicLock();
	this->busy = 1;
	this->run = 0;
}
void multiThreading_base::synch_stop() {
	this->busy = 0;
	this->basicUnlock();
}
