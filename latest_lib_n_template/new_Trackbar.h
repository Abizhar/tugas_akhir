#pragma once
#if defined(_WIN32)
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#else
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#endif
#include <vector>

using namespace cv;
using namespace std;

namespace bical {
	class new_Trackbar {

	private:
		vector <int> *data;

	public:
		Scalar lower(); 
		Scalar upper();

		vector <int> get_vector();
		new_Trackbar(const char* _window_name, vector <int>* _data = new vector <int> {0, 0, 0, 255, 255, 255});
		~new_Trackbar();
	};
}

