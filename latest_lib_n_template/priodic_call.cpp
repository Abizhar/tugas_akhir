#include "periodic_call.h"

uint8_t periodic_call::call(uint32_t _period) {
    if (flag == 2) {  // pertama dipanggil
        flag = 0;
        prev = this->get_tick() + _period;
        return 1;
    }

    int64_t dur = (int64_t)this->get_tick() - prev;
    if (dur >= _period) {  // jika sesuai periode flag = 1
        flag = 1;
        prev += _period;
    } else
        flag = 0;

    if (flag == 1) {  // setelah eksekusi flag = 0
        flag = 0;
        return 1;
    } else
        flag = 0;

    return 0;
}
uint8_t periodic_call::restart() {
    flag = 2;
    return 0;
}
uint32_t periodic_call::get_tick() {
    chrono::duration<double> c;
    now = chrono::high_resolution_clock::now();
    c   = now - start;
    return c.count() * 1000;
}
uint32_t periodic_call::elapsed() {
    uint32_t diff = this->get_tick() - last;
    last          = this->get_tick();
    return diff;
}

periodic_call::periodic_call() {
    flag  = 2;
    start = chrono::high_resolution_clock::now();
}
periodic_call::~periodic_call() {
}