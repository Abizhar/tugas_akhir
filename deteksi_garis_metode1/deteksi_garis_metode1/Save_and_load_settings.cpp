#include "Save_and_load_settings.h"

int bical::load_settings(char* file_name, vector<double>* data) {
    ifstream file;
    file.open(file_name, ios::in);

    if (!file.is_open())
        return 0;

    for (unsigned int i = 0; i < data->size(); i++) {
        file >> data->at(i);
    }
    file.close();

    return 1;
}
int bical::save_settings(char* file_name, vector<double>* data) {
    ofstream file;
    file.open(file_name, ios::out);

    if (!file.is_open())
        return 0;

    for (unsigned int i = 0; i < data->size(); i++) {
        file << (int)data->at(i) << ' ';
    }
    file << endl;
    file.close();

    return 1;
}

int bical::load_settings(char* file_name, vector<int>* data) {
    ifstream file;
    file.open(file_name, ios::in);

    if (!file.is_open())
        return 0;

    for (unsigned int i = 0; i < data->size(); i++) {
        file >> data->at(i);
    }
    file.close();

    return 1;
}
int bical::save_settings(char* file_name, vector<int>* data) {
    ofstream file;
    file.open(file_name, ios::out);

    if (!file.is_open())
        return 0;

    for (unsigned int i = 0; i < data->size(); i++) {
        file << (int)data->at(i) << ' ';
    }
    file << endl;
    file.close();

    return 1;
}