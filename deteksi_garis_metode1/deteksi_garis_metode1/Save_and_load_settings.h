#pragma once

#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

namespace bical {
int load_settings(char* file_name, vector<double>* data);
int save_settings(char* file_name, vector<double>* data);

int load_settings(char* file_name, vector<int>* data);
int save_settings(char* file_name, vector<int>* data);
}  // namespace bical