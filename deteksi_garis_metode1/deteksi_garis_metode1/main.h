#ifndef MAIN_H_
#define MAIN_H_

#define CAMERA_CAPTURE			0
#define VIDEO_INPUT_DIR			"v2_editted.avi"
#define DISPLAY_ROI_GRID		1
#define DISPLAY_REG_POINT		1
#define DISPLAY_EST_LINE		1
#define DISPLAY_CENTER_MARKER	0
#define DISPLAY_OFF_CENTER		1
#define DISPLAY_OFF_VERTICAL	1
#define THRESHOLD_WINDOW		0
#define DEBUG_COUT_PERIOD		30
#define LOG_ESTIMATED_LINE		1
#define LOG_FNAME				"estimated_line.csv"

#define REGION_SPLIT		10

#if defined(_WIN32)
#define SETTING_DIR			"./../../settings/th_garis.ini"
#else
#define SETTING_DIR			"./../../../settings/th_garis.ini"
#endif

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include <iostream>

#include "new_Trackbar.h"
#include "Save_and_load_settings.h"
#include "periodic_call.h"
#include "movingAVG.h"
#include "log_file.h"

using namespace std;
using namespace cv;
using namespace bical;

VideoCapture cap;
Mat ori, garis, ori_hsv;

Moments mments[REGION_SPLIT];
Rect	ROI_rect[REGION_SPLIT];
Point	centroids[REGION_SPLIT];
Point	tl[REGION_SPLIT];
Point	br[REGION_SPLIT];

int frame_width, frame_height;

vector <int> th_garis;
vector <Point> regression_point;
typedef struct {
	double a;
	double b;
} line_des;

line_des detected_line;
line_des detected_line_filtered;
line_des regression(vector <Point> _in);

float fps;
float off_center, off_vertical;
float off_center_fil, off_vertical_fil;

mov_avg a, b;

periodic_call timer1;

#if LOG_ESTIMATED_LINE == 1
double log_time;
log_file log_est_line;
#endif // LOG_ESTIMATED_LINE == 1
#endif