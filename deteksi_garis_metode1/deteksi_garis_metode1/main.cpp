#include "main.h"

using namespace std;
using namespace bical;

int main() {
	//select video sources
	#if CAMERA_CAPTURE == 1
	cap.open(0);
	#else
	cap.open(VIDEO_INPUT_DIR);
	#endif // CAMERA_CAPTURE == 0

	cap.set(CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CAP_PROP_FRAME_HEIGHT, 480);

	frame_width = cap.get(CAP_PROP_FRAME_WIDTH);
	frame_height = cap.get(CAP_PROP_FRAME_HEIGHT);

	if (!cap.isOpened()) {
		cout << "opening video source failed! \n";
		return -1;
	}
	else
		cout << "video source opened \n";
	//select video sources

	//initialize settings and trackbar
	th_garis = vector <int>(6);
	if (!load_settings(SETTING_DIR, &th_garis)) {
		cout << "file not found, creating new file \n";
		save_settings(SETTING_DIR, &th_garis);
	}
	new_Trackbar trackbar_garis("trackbar_garis", &th_garis);
	//initialize settings and trackbar

	//initialize region
	for (uint8_t i = 0; i < REGION_SPLIT; i++) {
		tl[i] = Point(0, frame_height*i / REGION_SPLIT);
		br[i] = Point(frame_width, frame_height*(i+1) / REGION_SPLIT);

		ROI_rect[i] = Rect(tl[i], br[i]);
	}
	//initialize region
	a = mov_avg(5);
	b = mov_avg(5);

	#if LOG_ESTIMATED_LINE == 1
	log_time = 0;
	vector <string> title;
	title.push_back(string("time"));
	title.push_back(string("a_raw"));
	title.push_back(string("a_fil"));
	title.push_back(string("b_raw"));
	title.push_back(string("b_fil"));
	title.push_back(string("off_vertical_raw"));
	title.push_back(string("off_vertical_fil"));
	title.push_back(string("off_center_raw"));
	title.push_back(string("off_center_fil"));
	log_est_line.open_log(LOG_FNAME, title);
	#endif // LOG_ESTIMATED_LINE == 1

	while (true) {
		// fps calculation
		fps = 1000/timer1.elapsed();
		// fps calculation

		cap.read(ori);
		#if CAMERA_CAPTURE == 0
		if (ori.empty()) {
			cap.open(VIDEO_INPUT_DIR);
			cout << "video source opened \n";
			cap.read(ori);
		}
		#endif

		cvtColor(ori, ori_hsv, CV_BGR2HSV);
		inRange(ori_hsv, trackbar_garis.lower(), trackbar_garis.upper(), garis);

		regression_point = vector <Point>(0);

		//gathering regression point
		for (uint8_t i = 0; i < REGION_SPLIT; i++) {
			#if DISPLAY_ROI_GRID == 1
			rectangle(ori, ROI_rect[i], Scalar(180, 110, 120), 1);
			#endif			
			mments[i] = moments(garis(ROI_rect[i]), true);

			if (mments[i].m00 > 70) {
				centroids[i] = Point(mments[i].m10 / mments[i].m00, (mments[i].m01 / mments[i].m00) + tl[i].y);
				#if DISPLAY_REG_POINT == 1
				circle(ori, centroids[i], 8, Scalar(128, 0, 0), -1);
				#endif // DISPLAY_ROI_GRID == 1	
				regression_point.push_back(centroids[i]);
			}
		}
		//gathering regression point

		//line estimation
		if (regression_point.size() > 2) {
			detected_line = regression(regression_point);
			detected_line_filtered.a = a.get_avg(detected_line.a);
			detected_line_filtered.b = b.get_avg(detected_line.b);

			#if DISPLAY_EST_LINE == 1
			Point p0(((0 - detected_line.b) / detected_line.a), 0);
			Point p1(((frame_height - detected_line.b) / detected_line.a), frame_height);
			Point p2(((0 - detected_line_filtered.b) / detected_line_filtered.a), 0);
			Point p3(((frame_height - detected_line_filtered.b) / detected_line_filtered.a), frame_height);

			line(ori, p2, p3, Scalar(128, 200, 200), 2);
			line(ori, p0, p1, Scalar(128, 200, 0), 2);
			#endif
		}
		//line estimation

		//calculate off_center & off_vertical
		off_center = (frame_width/2) - (((frame_height/2) - detected_line.b) / detected_line.a);
		off_vertical = (((frame_height/2) - detected_line.b) / detected_line.a) - ((0 - detected_line.b) / detected_line.a);

		off_center_fil = (frame_width / 2) - (((frame_height / 2) - detected_line_filtered.b) / detected_line_filtered.a);
		off_vertical_fil = (((frame_height / 2) - detected_line_filtered.b) / detected_line_filtered.a) - ((0 - detected_line_filtered.b) / detected_line_filtered.a);
		Point p0, p1;
		#if DISPLAY_OFF_CENTER == 1
		p0 = Point(frame_width/2, frame_height/2);
		p1 = Point((((frame_height/2) - detected_line.b) / detected_line.a), frame_height/2);
		circle(ori, p0, 8, Scalar(128, 100, 0), -1);
		circle(ori, p1, 8, Scalar(128, 100, 0), -1);
		line(ori, p0, p1, Scalar(0, 0, 128), 2);
		#endif

		#if DISPLAY_OFF_VERTICAL == 1
		p0 = Point((((frame_height / 2) - detected_line.b) / detected_line.a), 0);
		p1 = Point(((0 - detected_line.b) / detected_line.a), 0);
		circle(ori, p0, 8, Scalar(128, 100, 0), -1);
		circle(ori, p1, 8, Scalar(128, 100, 0), -1);
		line(ori, p0, p1, Scalar(0, 0, 128), 5);
		#endif
		//calculate off_center & off_vertical

		//resize(ori, ori, Size(640 * 2, 480 * 2));
		imshow("ori", ori);
		#if THRESHOLD_WINDOW == 1
		imshow("garis", garis);
		#endif
		//print routine
		if (timer1.call(DEBUG_COUT_PERIOD)) {
			//cout << "fps = " << fps << endl;
			cout << off_center << " " << off_vertical << endl;

			#if LOG_ESTIMATED_LINE == 1
			vector<double> line_data;
			line_data.push_back(log_time);
			line_data.push_back(detected_line.a);
			line_data.push_back(detected_line_filtered.a);
			line_data.push_back(detected_line.b);
			line_data.push_back(detected_line_filtered.b);
			line_data.push_back(off_vertical);
			line_data.push_back(off_vertical_fil);
			line_data.push_back(off_center);
			line_data.push_back(off_center_fil);
			log_est_line.log(line_data);
			log_time += DEBUG_COUT_PERIOD;
			#endif
		}
		//print routine
		switch (waitKey(1)) {
		case 27:
			return 0;
			break;

		case 's':
			cout << "saved \n";
			save_settings(SETTING_DIR, &th_garis);
			break;

		case 'r':
			cout << "reloaded \n";
			load_settings(SETTING_DIR, &th_garis);
			break;
		}
	}
}

line_des regression(vector <Point> _in) {
	double sigXi	= 0;
	double sigYi	= 0;
	double sigXisq	= 0;
	double sigXiYi	= 0;
	double Xbar		= 0;
	double Ybar		= 0;

	for (uint8_t i = 0; i < _in.size(); i++) {
		sigXi += _in[i].x;
		sigYi += _in[i].y;
		sigXisq += (_in[i].x*_in[i].x);
		sigXiYi += (_in[i].x*_in[i].y);
	}

	Xbar = sigXi / _in.size();
	Ybar = sigYi / _in.size();

	line_des ret_val;
	ret_val.a = ((_in.size()*sigXiYi) - (sigXi*sigYi)) / ((_in.size()*sigXisq) - (sigXi*sigXi));
	ret_val.b = Ybar - (ret_val.a*Xbar);
	return ret_val;
}