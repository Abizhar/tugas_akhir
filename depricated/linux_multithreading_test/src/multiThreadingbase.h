#ifndef MULTITHREADINGBASE_H_
#define MULTITHREADINGBASE_H_

#include <thread>
#include <mutex>
#include <iostream>

using namespace std;

class multiThreading_base {
public:
	multiThreading_base();
	virtual ~multiThreading_base();

	int startThread();

	int waitThread();

	virtual void threadedFuntion();

	void basicLock();
	void basicUnlock();

	int running = 0;
private:
	thread th;
protected:
	mutex locker;
};

#endif /* MULTITHREADINGBASE_H_ */
