/*
 * counter.h
 *
 *  Created on: Oct 6, 2018
 *      Author: dnne
 */

#ifndef COUNTER_H_
#define COUNTER_H_
#include "multiThreadingbase.h"
#include <iostream>

using namespace std;

class counter : public multiThreading_base{
public:
	counter();
	~counter();
	void threadedFuntion();
private:
};

#endif /* COUNTER_H_ */
