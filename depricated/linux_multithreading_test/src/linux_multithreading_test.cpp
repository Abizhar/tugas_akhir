#include <iostream>
#include "userCode.h"

int exit_code;

int main() {
	_setup();

	while(!exit_code){
		_loop();
	}

	_exit();
	return exit_code;
}
