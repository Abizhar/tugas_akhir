#ifndef USERCODE_H_
#define USERCODE_H_

#include <iostream>
#include "multiThreadingbase.h"
#include "counter.h"

using namespace std;

extern int exit_code;

void _setup();
void _loop();
void _exit();

#endif /* USERCODE_H_ */
