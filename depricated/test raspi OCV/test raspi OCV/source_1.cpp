#if defined(_WIN32)
	#include <opencv2\core\core.hpp>
	#include <opencv2\highgui\highgui.hpp>
	#include <opencv2\imgproc\imgproc.hpp>
#else
	#include <opencv2/core.hpp>
	#include <opencv2/highgui.hpp>
	#include <opencv2/imgproc.hpp>
#endif

#include <iostream>
#include <chrono>

using namespace cv;
using namespace std;

int main() {
	VideoCapture cap(0);
	Mat ori;

	chrono::time_point<chrono::high_resolution_clock> start, end;

	cap.set(CAP_PROP_FRAME_WIDTH, 640);
	cap.set(CAP_PROP_FRAME_HEIGHT, 480);
	cap.set(CAP_PROP_FPS, 90);

	if (!cap.isOpened())
		return -1;

	while (true) {
		start = chrono::high_resolution_clock::now();
		cap.read(ori);
		imshow("ori", ori);
		switch (waitKey(1)) {
		case 27:
			return 1;
			break;
		}
		end = chrono::high_resolution_clock::now();
		chrono::duration<double> c = end - start;
		cout << 1000/(c.count()*1000.0f) << endl;
	}
	return 0;
}
