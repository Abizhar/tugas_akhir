#include "PID.h"

PID::PID() {}
PID::PID(double _kp, double _ki, double _kd, double _sym_out_limit, double _looptime) {
	error = 0;
	if (_sym_out_limit >= 0) {
		upper_limit = _sym_out_limit;
		lower_limit = -_sym_out_limit;
	}
	else
		no_limit = true;

	P = 0;
	I = 0;
	D = 0;
	kp = _kp;
	ki = _ki;
	kd = _kd;

	this->looptime = _looptime;
	this->setPoint = 0, target = 0;
	this->dOnInput = false;
}
PID::PID(vector<double> _kPID, double _sym_out_limit, double _looptime) {
	error = 0;
	if (_sym_out_limit >= 0) {
		upper_limit = _sym_out_limit;
		lower_limit = -_sym_out_limit;
	}
	else
		no_limit = true;

	P = 0;
	I = 0;
	D = 0;
	kp = _kPID[0];
	ki = _kPID[1];
	kd = _kPID[2];

	this->looptime = _looptime;
	this->setPoint = 0, target = 0;
	this->dOnInput = false;
}

double PID::compute(double _setPoint, double _input) {
	this->setPoint = _setPoint;
	error = this->setPoint - _input;

	P = kp*error;
	if (dOnInput)
		D = kd* (error - last_error) / looptime;
	else
		//D = -kd* (_input - last_input) / looptime;
		D = kd* (last_input - _input) / looptime;
	last_error = error;
	last_input = _input;

	double PID = P + D;

	if (no_limit) {
		I += ki*error*looptime;
		return PID + I;
	}
	else {
		if (PID >= upper_limit)
			return upper_limit;
		if (PID <= lower_limit)
			return lower_limit;

		I += ki*error*looptime;
		PID += I;

		if (PID >= upper_limit)
			return upper_limit;
		if (PID <= lower_limit)
			return lower_limit;

		return PID;
	}
}
double PID::compute(double _input) {
	error = this->setPoint - _input;

	P = kp*error;
	if(dOnInput)
		D = kd* (error - last_error) / looptime;
	else
		//D = -kd* (_input - last_input) / looptime;
		D = kd* (last_input - _input) / looptime;
	last_error = error;
	last_input = _input;

	double PID = P + D;

	if (no_limit) {
		I += ki*error*looptime;
		return PID + I;
	}
	else {
		if (PID >= upper_limit)
			return upper_limit;
		if (PID <= lower_limit)
			return lower_limit;

		I += ki*error*looptime;
		PID += I;

		if (PID >= upper_limit)
			return upper_limit;
		if (PID <= lower_limit)
			return lower_limit;

		return PID;
	}
}

void PID::set_limit(double _lower, double _upper) {
	upper_limit = _upper;
	lower_limit = _lower;
}
void PID::set_limit(double _limit) {
	upper_limit = _limit;
	lower_limit = -_limit;
}
void PID::set_kPID(double _kp, double _ki, double _kd) {
	kp = _kp;
	ki = _ki;
	kd = _kd;
}
vector<double> PID::get_PID() {
	return vector<double>(3) = { P, I, D };
}

PID::~PID() {
}
