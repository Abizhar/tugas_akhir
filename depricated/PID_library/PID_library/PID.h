#pragma once
#include <vector>

using namespace std;
class PID {
private:
	double error = 0, last_error = 0, last_input = 0;
	double upper_limit, lower_limit;
	double P, I, D, kp, ki, kd;
public:
	double looptime = 1;
	double setPoint = 0, target = 0;


	bool dOnInput = true;
	bool no_limit = false;

	PID();
	PID(double _kp, double _ki, double _kd, double _sym_out_limit = -1, double _looptime = 1);
	PID(vector<double> _kPID, double _sym_out_limit = -1, double _looptime = 1);

	void set_limit(double _lower, double _upper);
	void set_limit(double _limit);
	void set_kPID(double _kp, double _ki, double _kd);
	vector<double> get_PID();

	double compute(double _setPoint, double _input);
	double compute(double _input);
	~PID();
};

