#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
 
using namespace cv;
using namespace std;
 
int main(int argc,char ** argv){
  Mat img;
  img = imread("images.jpeg", CV_LOAD_IMAGE_COLOR);
  imshow("img", img);  
  waitKey(0);
  return 0;
}