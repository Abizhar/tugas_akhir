#include "multiThreadingbase.h"

multiThreading_base::multiThreading_base() {
	this->running = 0;
}

multiThreading_base::~multiThreading_base() {
	this->running = 0;

	if(th.joinable()){
		th.join();
	}
}

int multiThreading_base::startThread(){ // ret 1 if successuflly start a new thread, 0 if thread already running
	if(!running){
		this->running = 1;
		th = thread(&multiThreading_base::threadedFuntion, this);
		return 1;
	}
	else
		return 0;
}

int multiThreading_base::waitThread(){
	this->running = 0;
		if(th.joinable()){
			th.join();
			return 1;
		}
		else
			return 0;
}

void multiThreading_base::threadedFuntion(){

}

void multiThreading_base::basicLock(){
	locker.lock();
}
void multiThreading_base::basicUnlock(){
	locker.unlock();
}
