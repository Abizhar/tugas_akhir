#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include "multiThreadingbase.h"
#include "thread1.h"
 
using namespace cv;
using namespace std;
 
thread1 a;
int main(int argc,char ** argv){
  a.startThread();
  Mat img;
  img = imread("resource/images.jpeg", CV_LOAD_IMAGE_COLOR);
  imshow("img", img); 
	while(true){
		a.basicLock();
		cout << "main thread" << endl; 
		a.basicUnlock();
		if(waitKey(1) == 27) break;
	} 
  return 0;
}