#include "Save_and_load_settings.h"
#if defined(uint)
#else
	#define uint unsigned int
#endif


int bical::load_settings(char* file_name, vector<double>* data) {
    ifstream file;
    file.open(file_name, ios::in);

    if (!file.is_open())
        return 0;

    for (uint i = 0; i < data->size(); i++) {
        file >> data->at(i);
    }
    file.close();

    return 1;
}
int bical::load_settings(char* file_name, vector<string> *title, vector<double>* data) {
	ifstream file;
	file.open(file_name, ios::in);

	if (!file.is_open())
		return 0;

	for (uint i = 0; i < data->size(); i++) {
		file >> title->at(i);
		file >> data->at(i);
	}
	file.close();

	return 1;
}
int bical::save_settings(char* file_name, vector<double> data) {
    ofstream file;
    file.open(file_name, ios::out);

    if (!file.is_open())
        return 0;

    for (uint i = 0; i < data.size(); i++) {
        file << data.at(i) << ' ';
    }
    file << endl;
    file.close();

    return 1;
}
int bical::save_settings(char* file_name, vector<string> title, vector<double> data) {
	ofstream file;
	file.open(file_name, ios::out);

	if (!file.is_open())
		return 0;
	if (title.size() != data.size())
		return -1;

	for (uint i = 0; i < data.size(); i++) {
		file << title.at(i) << '\t' << data.at(i) << endl;
	}
	file.close();

	return 1;
}

int bical::load_settings(char* file_name, vector<int>* data) {
	ifstream file;
	file.open(file_name, ios::in);

	if (!file.is_open())
		return 0;

	for (uint i = 0; i < data->size(); i++) {
		file >> data->at(i);
	}
	file.close();

	return 1;
}
int bical::load_settings(char* file_name, vector<string> *title, vector<int>* data) {
	ifstream file;
	file.open(file_name, ios::in);

	if (!file.is_open())
		return 0;

	for (uint i = 0; i < data->size(); i++) {
		file >> title->at(i);
		file >> data->at(i);
	}
	file.close();

	return 1;
}
int bical::save_settings(char* file_name, vector<int> data) {
    ofstream file;
    file.open(file_name, ios::out);

    if (!file.is_open())
        return 0;

    for (uint i = 0; i < data.size(); i++) {
        file << (int)data.at(i) << ' ';
    }
    file << endl;
    file.close();

    return 1;
}
int bical::save_settings(char* file_name, vector<string> title, vector<int> data) {
	ofstream file;
	file.open(file_name, ios::out);

	if (!file.is_open())
		return 0;
	if (title.size() != data.size())
		return -1;

	for (uint i = 0; i < data.size(); i++) {
		file << title.at(i) << '\t' << data.at(i) << endl;
	}
	file.close();

	return 1;
}