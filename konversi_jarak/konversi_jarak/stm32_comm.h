#ifndef STM32_COMM_H_
#define STM32_COMM_H_

#include <errno.h>
#include <unistd.h>
#include <wiringSerial.h>
#include "multiThreadingbase.h"
#include "periodic_call.h"

#define HEADER_1 0x20
#define HEADER_2 0x40

using namespace std;

class thread1 : public multiThreading_base {
   private:
    periodic_call RC_TX_timer, RC_RX_timer;
    void send_buffer(int _device, uint8_t* _buff, int _buff_sz);
    uint8_t receive_buffer(int _device, uint8_t* _buff, int _buff_sz);
    int encode_Ibus(uint16_t* data_in, uint8_t* data_out);
    int decode_Ibus(uint8_t* data_in, uint16_t* data_out);

   public:
    uint8_t RC_in_buff[32];
    uint16_t RC_in_data[14];
    uint8_t RC_out_buff[32];
    uint16_t RC_out_data[14];

    void threadedFuntion();
};

#endif
