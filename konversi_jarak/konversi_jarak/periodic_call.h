#ifndef PERIODIC_CALL_H_
#define PERIODIC_CALL_H_
#include <chrono>

using namespace std;
class periodic_call {
   private:
    chrono::time_point<chrono::high_resolution_clock> start, now;
    uint8_t flag;
    uint32_t prev, last;

   public:
    uint8_t call(uint32_t _period);
    uint8_t restart();
    uint32_t get_tick();
    uint32_t elapsed();

    periodic_call();
    ~periodic_call();
};

#endif