#include "stm32_comm.h"

void thread1::threadedFuntion() {
    auto serial_port = serialOpen("/dev/ttyS0", 115200);
    if (serial_port < 0) {
        cout << "error opening serial port, exiting" << endl;
        this->quit();
    } else
        cout << "serial_opened" << endl;

    RC_TX_timer = periodic_call();
    RC_RX_timer = periodic_call();

    while (this->running == 1) {
        if (RC_TX_timer.call(6) == 1) {
            this->basicLock();
            encode_Ibus(RC_out_data, RC_out_buff);
            this->basicUnlock();

            send_buffer(serial_port, RC_out_buff, 32);
        }

        if (RC_RX_timer.call(3) == 1) {
            uint8_t rx_data_avail;
            int8_t decode_ibus_ret;

            rx_data_avail = receive_buffer(serial_port, RC_in_buff, 32);

            if (rx_data_avail == 1) {
                this->basicLock();
                decode_ibus_ret = decode_Ibus(RC_in_buff, RC_in_data);
                this->basicUnlock();
            }

            if (decode_ibus_ret == -1 || decode_ibus_ret == -3)
                serialFlush(serial_port);
        }
    }
    this->quit();
}

void thread1::send_buffer(int _device, uint8_t* _buff, int _buff_sz) {
    write(_device, _buff, _buff_sz);
}

uint8_t thread1::receive_buffer(int _device, uint8_t* _buff, int _buff_sz) {
    if (serialDataAvail(_device) >= _buff_sz) {
        read(_device, _buff, _buff_sz);
        return 1;
    } else
        return 0;
}

int thread1::encode_Ibus(uint16_t* data_in, uint8_t* data_out) {
    data_out[0] = HEADER_1;
    data_out[1] = HEADER_2;

    for (uint16_t i = 0; i < 14; i++) {
        data_out[2 * (i + 1) + 1] = data_in[i] >> 8;
        data_out[2 * (i + 1)]     = data_in[i] & 255;
    }

    uint16_t buffSum = 0xffff;
    for (uint16_t i = 0; i < 30; i++)
        buffSum -= *(data_out + i);

    data_out[31] = buffSum >> 8;
    data_out[30] = buffSum & 255;
    return 0;
}

int thread1::decode_Ibus(uint8_t* data_in, uint16_t* data_out) {
    uint16_t buffSum = 0xffff, cksum;

    if (*data_in == HEADER_1 && *(data_in + 1) == HEADER_2) {
        cksum = *(data_in + 31) << 8;
        cksum |= *(data_in + 30);

        for (uint16_t i = 0; i < 30; i++)
            buffSum -= *(data_in + i);

        if (buffSum != cksum) {
            data_in[0] = 0xfd;
            data_in[1] = 0xff;
            return -1;  // checksum missmatch
        }

        for (uint16_t i = 0; i < 14; i++) {
            *(data_out + i) = *(data_in + (2 * (i + 1)) + 1) << 8;
            *(data_out + i) |= *(data_in + (2 * (i + 1)));
        }

        data_in[0] = 0xfd;
        data_in[1] = 0xff;
        return 0;  // data 0K
    }

    else if (*data_in == 0xfd && *(data_in + 1) == 0xff)
        return -2;  // empty data

    else {
        data_in[0] = 0xfd;
        data_in[1] = 0xff;
        return -3;  // invalid header
    }
}