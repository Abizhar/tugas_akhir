#if defined(_WIN32)
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#define SETTING_DIR "./../th.ini"
#define SETTING_DIR1 "./../d.ini"
#else
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#define SETTING_DIR "./../../th.ini"
#define SETTING_DIR1 "./../../d.ini"
#endif

#include <iostream>
#include "Save_and_load_settings.h"
#include "new_Trackbar.h"
#include "stm32_comm.h"

using namespace cv;
using namespace std;
using namespace bical;

int main(int argc, char **argv) {
	thread1 STM32_comm_thread;
	STM32_comm_thread.startThread();
	int16_t ketinggian = 0;
	double jarak_cm;

	Mat ori, ori_hsv;
	Mat th1;

	VideoCapture cap;
	cap.open(0);

	vector<int> th_val(6);
	vector<int> d(1);
	load_settings(SETTING_DIR, &th_val);
	load_settings(SETTING_DIR1, &d);
	new_Trackbar trackbar1("th", &th_val);

	while (true) {
		STM32_comm_thread.basicLock();
		ketinggian = STM32_comm_thread.RC_in_data[10];
		STM32_comm_thread.basicUnlock();

		cap.read(ori);
		resize(ori, ori, Size(320, 240));
		cvtColor(ori, ori_hsv, CV_BGR2HSV);
		inRange(ori_hsv, trackbar1.lower(), trackbar1.upper(), th1);

		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;
		double jarak = 0;

		findContours(th1, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
		
		if (contours.size() >= 2) {
			vector<Point2f>center(2);
			vector<float>radius(2);
			Point2f temp_center;
			float temp_radius;
			vector<Point> contours_poly;

			for (int j = 0; j < contours.size(); j++) {// loop thrugh all contours
				approxPolyDP(Mat(contours[j]), contours_poly, 3, true);
				minEnclosingCircle((Mat)contours_poly, temp_center, temp_radius);

				if (temp_radius > radius[0]) {
					radius[1] = radius[0];
					center[1] = center[0];
					radius[0] = temp_radius;
					center[0] = temp_center;
				}
				else if (temp_radius > radius[1]) {
					radius[1] = temp_radius;
					center[1] = temp_center;
				}
			}

			circle(ori, center[0], (int)radius[0] + 10, Scalar(255, 50, 0), 2);
			circle(ori, center[1], (int)radius[1] + 10, Scalar(255, 50, 0), 2);
			line(ori, center[0], center[1], Scalar(0, 0, 200), 2);
			jarak = pow((center[0].x - center[1].x), 2) + pow((center[0].y - center[1].y), 2);
			jarak = sqrt(jarak);
		}
		double divider;
		ketinggian = d[0];
		divider = 278.5246148 * exp(-0.01911796479 * ketinggian);
		divider += 30;
		divider = 28.8/ divider;
		jarak_cm = (double) jarak*divider;

		cout << ketinggian << "\t" << jarak_cm << endl;
		imshow("threshold", th1);
		imshow("ori", ori);
		switch (waitKey(1)) {
		case 27:
			return 0;
			break;
		case 's':
			save_settings(SETTING_DIR, th_val);
			break;
		}
	}
}