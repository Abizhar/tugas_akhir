#pragma once

#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

namespace bical {
int load_settings(char* file_name, vector<double>* data);
int load_settings(char* file_name, vector<string>* title, vector<double>* data);
int save_settings(char* file_name, vector<double> data);
int save_settings(char* file_name, vector<string> title, vector<double> data);

int load_settings(char* file_name, vector<int>* data);
int load_settings(char* file_name, vector<string>* title, vector<int>* data);
int save_settings(char* file_name, vector<int> data);
int save_settings(char* file_name, vector<string> title, vector<int> data);
}  // namespace bical