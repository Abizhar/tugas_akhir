#include "main.h"

using namespace std;
using namespace bical;

int main() {
	#if CAMERA_CAPTURE == 1
	cap.open(0);
	#else
	cap.open(VIDEO_INPUT_DIR);
	#endif // CAMERA_CAPTURE == 0

	th_garis = vector <int>(6);
	if (load_settings(SETTING_DIR, &th_garis)) {
		cout << "file not found, creating new file \n";
		save_settings(SETTING_DIR, &th_garis);
	}
	new_Trackbar trackbar_garis("trackbar_garis", &th_garis);

	if (!cap.isOpened()) {
		cout << "opening video source failed! \n";
		return -1;
	}
	else
		cout << "video source opened \n";

	cout << "r : reload setting" << endl;
	cout << "s : save setting" << endl;
	cout << "q : next frame" << endl;
	cout << "w : resume" << endl;
	wait_key_time = 1;

	while (true) {
		cap.read(ori);
		#if CAMERA_CAPTURE == 0
		if (ori.empty()) {
			cap.open(VIDEO_INPUT_DIR);
			cout << "video source opened \n";
			cap.read(ori);
		}
		#endif
		cvtColor(ori, ori_hsv, CV_BGR2HSV);

		inRange(ori_hsv, trackbar_garis.lower(), trackbar_garis.upper(), garis);
		imshow("ori", ori);
		imshow("garis", garis);
		
		switch (waitKey(wait_key_time)) {
		case 27:
			return 0;
			break;

		case 's':
			cout << "saved \n";
			save_settings(SETTING_DIR, &th_garis);
			break;

		case 'r':
			cout << "reloaded \n";
			load_settings(SETTING_DIR, &th_garis);
			break;
		case 'q':
			wait_key_time = 0;
			break;
		case 'w':
			wait_key_time = 1;
			break;
		}
	}
}