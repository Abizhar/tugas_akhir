#ifndef MAIN_H_
#define MAIN_H_

#define CAMERA_CAPTURE 1

#define VIDEO_INPUT_DIR "11.avi"

#if defined(_WIN32)
#define SETTING_DIR "./../../settings/th_garis.ini"
#else
#define SETTING_DIR "./../../../settings/th_garis.ini"
#endif

#include <iostream>
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/tracking.hpp"

#include "Save_and_load_settings.h"
#include "new_Trackbar.h"

using namespace std;
using namespace cv;
using namespace bical;

VideoCapture cap;
Mat ori, garis, ori_hsv;

vector<int> th_garis;
int wait_key_time;
#endif