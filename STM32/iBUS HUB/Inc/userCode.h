/*
 * userCode.h
 *
 *  Created on: Jan 29, 2019
 *      Author: Muhammad Abizhar Mul
 */

#ifndef USERCODE_H_
#define USERCODE_H_

#include "main.h"
#include "stm32f1xx_hal.h"
#include <math.h>

#define HEADER_1 0x20
#define HEADER_2 0x40
#define CONTROL_DELAY 1000

#define RX	0
#define RPI	1

/////////////////////////////////////////////////////
typedef struct {
	uint8_t flag;
	uint32_t prev;
}perio;

typedef struct {
	uint16_t data;
	uint8_t cnt;
	perio tim;
}buzzer;
/////////////////////////////////////////////////////
I2C_HandleTypeDef hi2c1;
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;
DMA_HandleTypeDef hdma_usart3_rx;

WWDG_HandleTypeDef hwwdg;

int setup();
int loop();

int decode_Ibus(uint8_t *data_in, uint16_t* data_out);
int encode_Ibus(uint16_t *data_in, uint8_t* data_out);
int send_mu2fc(uint16_t *channel_data, uint8_t *buffer);
int send_mu2rpi(uint16_t *channel_data, uint8_t *buffer);

int periodic_call(perio *perio_struct, uint32_t period);
void refresh_periodic_call(perio *perio_struct);
int initiate_periodic_call(perio *perio_struct);

int delayed_call(perio *perio_struct, uint32_t _delay);
void refresh_delayed_call(perio *perio_struct);

int input_changed(uint16_t *_data, uint16_t *_last_data,uint16_t _thr);

void buzzer_send(buzzer* buzzer_data);
void buzzer_callback(buzzer* buzzer_data);
/////////////////////////////////////////////////////

uint8_t rx2mu_buffer [32];
uint16_t rx2mu_data [14];
uint16_t prev_rx2mu_data [14];
uint8_t mu2fc_buffer [32];
uint16_t mu2fc_data [14];
uint16_t last_armed;

uint8_t mu2rpi_buffer [32];
uint16_t mu2rpi_data [14];
uint8_t rpi2mu_buffer [32];
uint16_t rpi2mu_data [14];

uint16_t rx_OK, rx_missmatch, rx_empty, rx_inv;
int16_t rx_error;
float rx_error_perc;

uint16_t rpi_OK, rpi_missmatch, rpi_empty, rpi_inv;
int16_t rpi_error;
float rpi_error_perc;

perio rx2mu_request_call, mu2fc_send_call, rpi2mu_request_call, mu2rpi_send_call;
perio target_rate;
perio mu2raspi_fail;
perio control_delay;

buzzer buzzer_on;
buzzer buzzer_rpi, buzzer_rx;

uint32_t target_count;
uint32_t WWDG_periode, WWDG_prev_time;

uint8_t rx_lost_flag;
uint8_t rpi_rx_flag;
uint8_t control_flag, last_control_flag;

uint32_t debug_tx;
///////////////////////////////////////////////////////////////
#define FIL_SZ 250
I2C_HandleTypeDef hi2c1;
typedef struct {
	uint16_t dig_T1;
	int16_t dig_T2, dig_T3;
} temp_param;

typedef struct {
	uint16_t dig_P1;
	int16_t dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;
} pres_param;

void bmp_init(uint8_t dev_addr, I2C_HandleTypeDef hi2c1);
pres_param get_pressure_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c1);
temp_param get_temperature_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c1);

double get_temperature(uint8_t dev_addr, temp_param temp_param, int32_t* t_fine, I2C_HandleTypeDef hi2c1);
double get_pressure(uint8_t dev_addr, pres_param pres_param, int32_t t_fine, I2C_HandleTypeDef hi2c1);
double get_altitude(double pressure, double ref_alt);
double mov_avg(double in);

pres_param pressure_param;
temp_param temperature_param;
double temperature, pressure, altitude, ref_altitude, altitude_fil;
int32_t t_fine;
///////////////////////////////////////////////////////////////
#endif /* USERCODE_H_ */
