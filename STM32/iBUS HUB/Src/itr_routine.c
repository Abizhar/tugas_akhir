/*
 * itr_routine.c
 *
 *  Created on: Feb 1, 2019
 *      Author: Muhammad Abizhar Mul
 */
#include "userCode.h"

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART3){ //terima data dari receiver
		HAL_WWDG_Refresh(&hwwdg);
		rx_lost_flag = 0;

		switch(decode_Ibus(rx2mu_buffer, rx2mu_data)){
		case 0: //OK
			rx_OK ++;
			break;
		case -1: //cksum missmatch
			rx_missmatch ++;
			break;
		case -2: //empty data
			rx_empty ++;
			break;
		case -3: //invalid header
			rx_inv ++;
			break;
		}

		if(control_flag == RPI)
			send_mu2fc(rpi2mu_data, mu2fc_buffer);
		else
			send_mu2fc(rx2mu_data, mu2fc_buffer);
	}

	if(huart->Instance == USART2){ //terima data dari rpi
		switch(decode_Ibus(rpi2mu_buffer, rpi2mu_data)){
		case 0: //OK
			rpi_OK ++;
			break;
		case -1: //cksum missmatch
			rpi_missmatch ++;
			break;
		case -2: //empty data
			rpi_empty ++;
			break;
		case -3: //invalid header
			rpi_inv ++;
			break;
		}

		rpi_rx_flag = 0;

		if(control_flag == RPI)
			rx2mu_data[8] = 1100;
		else
			rx2mu_data[8] = 1200;
		send_mu2rpi(rx2mu_data, mu2rpi_buffer);
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART2){
		refresh_periodic_call(&mu2raspi_fail);
	}
}

void HAL_WWDG_EarlyWakeupCallback(WWDG_HandleTypeDef *hwwdg){
	WWDG_periode = HAL_GetTick() - WWDG_prev_time;
	WWDG_prev_time = HAL_GetTick();
	HAL_WWDG_Refresh(hwwdg);
	rx_lost_flag = 1;
}
