/*
 * userCode.h
 *
 *  Created on: May 12, 2019
 *      Author: Muhammad Abizhar Mul
 */

#ifndef USERCODE_H_
#define USERCODE_H_

#include "main.h"
#include "stm32f1xx_hal.h"
#include <math.h>

#define FIL_SZ 100

I2C_HandleTypeDef hi2c1;

void setup();
void loop();

typedef struct {
	uint16_t dig_T1;
	int16_t dig_T2, dig_T3;
} temp_param;

typedef struct {
	uint16_t dig_P1;
	int16_t dig_P2, dig_P3, dig_P4, dig_P5, dig_P6, dig_P7, dig_P8, dig_P9;
} pres_param;

void bmp_init(uint8_t dev_addr, I2C_HandleTypeDef hi2c1);
pres_param get_pressure_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c1);
temp_param get_temperature_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c1);

double get_temperature(uint8_t dev_addr, temp_param temp_param, int32_t* t_fine, I2C_HandleTypeDef hi2c1);
double get_pressure(uint8_t dev_addr, pres_param pres_param, int32_t t_fine, I2C_HandleTypeDef hi2c1);
double get_altitude(double pressure, double ref_alt);
double mov_avg(double in);

#endif /* USERCODE_H_ */
