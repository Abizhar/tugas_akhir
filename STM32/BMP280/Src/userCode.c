/*
 * userCode.c
 *
 *  Created on: May 12, 2019
 *      Author: Muhammad Abizhar Mul
 */

#include "userCode.h"

uint8_t dev_addr = 0xEC;
pres_param pressure_param;
temp_param temperature_param;
double temperature, pressure, altitude, ref_altitude, altitude_fil;
int32_t t_fine;
int32_t time;
void setup() {
	bmp_init(dev_addr, hi2c1);
	pressure_param = get_pressure_parameter(dev_addr, hi2c1);
	temperature_param = get_temperature_parameter(dev_addr, hi2c1);

	temperature = get_temperature(dev_addr, temperature_param, &t_fine, hi2c1);
	pressure = get_pressure(dev_addr, pressure_param, t_fine, hi2c1);
	HAL_GPIO_WritePin(onBoardGPIO_GPIO_Port, onBoardGPIO_Pin, 0);
	for(int i = 0; i < FIL_SZ; i++){
		altitude = get_altitude(pressure, 0);
		altitude_fil = mov_avg(altitude);
		HAL_Delay(10);
	}
	HAL_GPIO_WritePin(onBoardGPIO_GPIO_Port, onBoardGPIO_Pin, 1);
	ref_altitude = altitude_fil;
}

void loop() {
	int32_t t1 = HAL_GetTick();
	temperature = get_temperature(dev_addr, temperature_param, &t_fine, hi2c1);
	pressure = get_pressure(dev_addr, pressure_param, t_fine, hi2c1);
	altitude = get_altitude(pressure, ref_altitude);
	altitude_fil = mov_avg(altitude);
	time = HAL_GetTick() - t1;

	HAL_Delay(1);
}

void bmp_init(uint8_t dev_addr, I2C_HandleTypeDef hi2c) {
	uint8_t I2C_tx_buff[3];
	I2C_tx_buff[0] = 0xF4;
	I2C_tx_buff[1] = 0x57;
	I2C_tx_buff[2] = 0x1C;
//	I2C_tx_buff[2] = 0x00;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 3, 20);
}

pres_param get_pressure_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[18];
	uint8_t I2C_tx_buff[1];
	uint16_t temp;

	pres_param ret_val;

	I2C_tx_buff[0] = 0x8E;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 18, 100);

	ret_val.dig_P1 = I2C_rx_buff[1] << 8;
	ret_val.dig_P1 |= I2C_rx_buff[0];

	temp = I2C_rx_buff[3] << 8;
	temp |= I2C_rx_buff[2];
	ret_val.dig_P2 = (int16_t) temp;

	temp = I2C_rx_buff[5] << 8;
	temp |= I2C_rx_buff[4];
	ret_val.dig_P3 = (int16_t) temp;

	temp = I2C_rx_buff[7] << 8;
	temp |= I2C_rx_buff[6];
	ret_val.dig_P4 = (int16_t) temp;

	temp = I2C_rx_buff[9] << 8;
	temp |= I2C_rx_buff[8];
	ret_val.dig_P5 = (int16_t) temp;

	temp = I2C_rx_buff[11] << 8;
	temp |= I2C_rx_buff[10];
	ret_val.dig_P6 = (int16_t) temp;

	temp = I2C_rx_buff[13] << 8;
	temp |= I2C_rx_buff[12];
	ret_val.dig_P7 = (int16_t) temp;

	temp = I2C_rx_buff[15] << 8;
	temp |= I2C_rx_buff[14];
	ret_val.dig_P8 = (int16_t) temp;

	temp = I2C_rx_buff[17] << 8;
	temp |= I2C_rx_buff[16];
	ret_val.dig_P9 = (int16_t) temp;

	return ret_val;
}

temp_param get_temperature_parameter(uint8_t dev_addr, I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[6];
	uint8_t I2C_tx_buff[1];
	uint16_t temp;

	temp_param ret_val;

	I2C_tx_buff[0] = 0x88;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 6, 100);

	ret_val.dig_T1 = I2C_rx_buff[1] << 8;
	ret_val.dig_T1 |= I2C_rx_buff[0];

	temp = I2C_rx_buff[3] << 8;
	temp |= I2C_rx_buff[2];
	ret_val.dig_T2 = (int16_t) temp;

	temp = I2C_rx_buff[5] << 8;
	temp |= I2C_rx_buff[4];
	ret_val.dig_T3 = (int16_t) temp;

	return ret_val;
}

double get_temperature(uint8_t dev_addr, temp_param temp_param, int32_t* t_fine,
		I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[3];
	uint8_t I2C_tx_buff[1];

	I2C_tx_buff[0] = 0xFA;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 3, 20);

	int32_t temp_ADC;

	temp_ADC = I2C_rx_buff[0];
	temp_ADC <<= 8;
	temp_ADC |= I2C_rx_buff[1];
	temp_ADC <<= 8;
	temp_ADC |= I2C_rx_buff[2];
	temp_ADC >>= 4;

	int32_t var1, var2;

	var1 = ((((temp_ADC >> 3) - ((int32_t) temp_param.dig_T1 << 1)))
			* ((int32_t) temp_param.dig_T2)) >> 11;

	var2 = (((((temp_ADC >> 4) - ((int32_t) temp_param.dig_T1))
			* ((temp_ADC >> 4) - ((int32_t) temp_param.dig_T1))) >> 12)
			* ((int32_t) temp_param.dig_T3)) >> 14;

	*t_fine = var1 + var2;

	double temperature;
	temperature = (*t_fine * 5 + 128) >> 8;
	temperature /= 100;

	return temperature;
}

double get_pressure(uint8_t dev_addr, pres_param pres_param, int32_t t_fine,
		I2C_HandleTypeDef hi2c) {
	uint8_t I2C_rx_buff[3];
	uint8_t I2C_tx_buff[1];

	I2C_tx_buff[0] = 0xF7;
	HAL_I2C_Master_Transmit(&hi2c, dev_addr, I2C_tx_buff, 1, 20);
	HAL_I2C_Master_Receive(&hi2c, dev_addr, I2C_rx_buff, 3, 20);

	int32_t pres_ADC;

	pres_ADC = I2C_rx_buff[0];
	pres_ADC <<= 8;
	pres_ADC |= I2C_rx_buff[1];
	pres_ADC <<= 8;
	pres_ADC |= I2C_rx_buff[2];
	pres_ADC >>= 4;

	int64_t var11, var22, p;

	var11 = ((int64_t) t_fine) - 128000;
	var22 = var11 * var11 * (int64_t) pres_param.dig_P6;
	var22 = var22 + ((var11 * (int64_t) pres_param.dig_P5) << 17);
	var22 = var22 + (((int64_t) pres_param.dig_P4) << 35);
	var11 = ((var11 * var11 * (int64_t) pres_param.dig_P3) >> 8)
			+ ((var11 * (int64_t) pres_param.dig_P2) << 12);
	var11 = (((((int64_t) 1) << 47) + var11)) * ((int64_t) pres_param.dig_P1)
			>> 33;

	double pressure;
	if (var11 != 0) {
		// avoid exception caused by division by zero
		p = 1048576 - pres_ADC;
		p = (((p << 31) - var22) * 3125) / var11;
		var11 = (((int64_t) pres_param.dig_P9) * (p >> 13) * (p >> 13)) >> 25;
		var22 = (((int64_t) pres_param.dig_P8) * p) >> 19;

		p = ((p + var11 + var22) >> 8) + (((int64_t) pres_param.dig_P7) << 4);
		pressure = (double) p / 256;
	}

	else
		pressure = -1.0;
	return pressure;
}

double get_altitude(double pressure, double ref_alt) {
	double altitude;
	altitude = 4433000 * (1.0 - pow((pressure / 100) / 1013.25, 0.1903));
	altitude -= ref_alt;
	return altitude;
}

double mov_avg(double in) {
	static ar[FIL_SZ];
	static i = 0;

	ar[i] = in;
	i++;
	if(i >= FIL_SZ)
		i = 0;

	double result = 0;
	for(int j = 0; j < FIL_SZ; j++)
		result += ar[j];

	result/=FIL_SZ;
	return result;
}
