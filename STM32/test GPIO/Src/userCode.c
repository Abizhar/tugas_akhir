/*
 * userCode.c
 *
 *  Created on: Feb 15, 2019
 *      Author: Muhammad Abizhar Mul
 */

#include "userCode.h"

int setup(){
	return 0;
}

int loop(){
	HAL_GPIO_TogglePin(onBoard_LED_GPIO_Port, onBoard_LED_Pin);
	HAL_Delay(200);
	return 0;
}
