/*
 * userCode.h
 *
 *  Created on: Feb 15, 2019
 *      Author: Muhammad Abizhar Mul
 */

#ifndef USERCODE_H_
#define USERCODE_H_

#include "main.h"
#include "stm32f1xx_hal.h"

int setup();
int loop();

#endif /* USERCODE_H_ */
