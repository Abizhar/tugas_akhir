#pragma once
#include <vector>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class log_file {
private:
	ofstream file;
public:
	log_file(char *_file_name); //open log file
	log_file(char *_file_name, vector <string> _title); //open log file
	log_file();

	int open_log(char *_file_name); //open log file
	int open_log(char *_file_name, vector <string> _title); //open and gives title

	int log(vector <double> _data);

	int close_log();

	~log_file();
};

