#include "new_Trackbar.h"

bical::new_Trackbar::new_Trackbar(const char* _window_name, vector <int>* _data) {
	data = _data;
	namedWindow(_window_name);
	createTrackbar("lh", _window_name, &data->at(0), 255);
	createTrackbar("uh", _window_name, &data->at(3), 255);
	createTrackbar("ls", _window_name, &data->at(1), 255);
	createTrackbar("us", _window_name, &data->at(4), 255);
	createTrackbar("lv", _window_name, &data->at(2), 255);
	createTrackbar("uv", _window_name, &data->at(5), 255);
}

Scalar bical::new_Trackbar::upper() {
	return Scalar(data->at(3), data->at(4), data->at(5));
}

Scalar bical::new_Trackbar::lower() {
	return Scalar(data->at(0), data->at(1), data->at(2));
}

vector <int> bical::new_Trackbar::get_vector() {
	return *data;
}

bical::new_Trackbar::~new_Trackbar() {
}
