#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/videoio.hpp"

#include <ctype.h>
#include <iostream>

#define CAM_CAPTURE 1
#define FILE_CNT	4
#define FILE_INDEX	1
#define ORIGIN
//#define DEST
//#define ARROW

using namespace cv;
using namespace std;

Point2f point;

typedef struct {
	double vx;
	double vy;
}vel;

vel get_vel(vector<Point2f> *p0, vector<Point2f> *p1, vector<uchar> *status);

int main() {
	int filenum = 0;
	VideoCapture cap;
	TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
	Size subPixWinSize(10, 10), winSize(31, 31);

	const int MAX_COUNT = 10;
	bool nightMode = false;

#if CAM_CAPTURE == 1
	cap.open(0);
	if (!cap.isOpened()) {
		cout << "Could not initialize capturing...\n";
		return -1;
	}
#else
	int file_num = FILE_INDEX;
#endif
	namedWindow("LK Demo", 1);

	Mat gray, prevGray, image, frame;
	vector<Point2f> points[2];

	for (;;) {
#if CAM_CAPTURE == 1
		cap >> image;
#else
		char file_name[10];
		snprintf(file_name, 10, "_%d.png", file_num);
		image = imread(file_name, CV_LOAD_IMAGE_COLOR);
		file_num++;
		if (file_num >= (FILE_INDEX + FILE_CNT))
			file_num = FILE_INDEX;
#endif
		

		cvtColor(image, gray, COLOR_BGR2GRAY);
		/*char _file_name[10];
		snprintf(_file_name, 10, "_%d.png", file_num);
		image = imread(_file_name, CV_LOAD_IMAGE_COLOR);
		file_num++;
		if (file_num >= (FILE_INDEX + FILE_CNT))
			file_num = FILE_INDEX;*/

		if (nightMode)
			image = Scalar::all(0);

		if (!points[0].empty()) { // jika point[0] tidak kosong
			vector<uchar> status;
			vector<float> err;

			if (prevGray.empty())
				gray.copyTo(prevGray);

			calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize, 3, termcrit, 0, 0.001);

			vel velocity = get_vel(&points[0], &points[1], &status);
			std::cout << velocity.vx << "	" << velocity.vy << std::endl;

			/*#if defined(DEST)
			for (int i = 0; i < points[0].size(); i++) {
				circle(image, points[0][i], 10, Scalar(0, 0, 255), -1);
			}
			#endif */

			size_t i, k;
			for (i = k = 0; i < points[1].size(); i++) {
				if (!status[i])
					continue;
				#if defined(ARROW)
				arrowedLine(image, points[0][i], points[1][i], Scalar(255, 0, 255), 8, 0, 0.4, 0.4);
				#endif
				
				points[1][k++] = points[1][i];
			}
			points[1].resize(k);
		}

		goodFeaturesToTrack(gray, points[1], MAX_COUNT, 0.01, 10);
		cornerSubPix(gray, points[1], subPixWinSize, Size(-1, -1), termcrit);
		#if defined(ORIGIN)
		for (int i = 0; i < points[1].size(); i++) {
			circle(image, points[1][i], 10, Scalar(0, 255, 30), -1);
		}
		#endif

		imshow("LK Demo", image);

		char c = (char)waitKey(1);
		if (c == 27)
			break;
		switch (c) {
		case 'n':
			nightMode = !nightMode;
			break;
		case 'c':
			char filname[20];
			snprintf(filname, 20, "ORIGIN_%d.png", filenum);
			filenum++;
			cout << filenum << "  captured \n";
			imwrite(filname, image);
			break;
		}
		

		std::swap(points[1], points[0]);
		cv::swap(prevGray, gray);
	}

	return 0;
}

vel get_vel(vector<Point2f> *p0, vector<Point2f> *p1, vector<uchar> *status) {
	vel velocity;
	velocity.vx = 0;
	velocity.vy = 0;
	int k = 0;
	for (int i = 0; i < p1->size(); i++) {
		if (!status->at(i))
			continue;

		velocity.vx += (p1->at(i).x - p0->at(i).x);
		velocity.vy += (p1->at(i).y - p0->at(i).y);
		k++;
	}
	velocity.vx /= k;
	velocity.vy /= k;

	return velocity;
}